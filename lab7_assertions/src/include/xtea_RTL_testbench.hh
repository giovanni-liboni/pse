#pragma once

#include "systemc.h"

SC_MODULE(xtea_RTL_testbench) {
private:
    void run();

public:
  	sc_out<sc_uint<32> >  word0;
  	sc_out<sc_uint<32> >  word1;

  	sc_out<sc_uint<32> >  key0;
  	sc_out<sc_uint<32> >  key1;
  	sc_out<sc_uint<32> >  key2;
  	sc_out<sc_uint<32> >  key3;

  	sc_in<sc_uint<32> >  res0;
  	sc_in<sc_uint<32> >  res1;

  	sc_out<bool>  	mode;

  	sc_in<bool>  	done;
  	sc_out<bool>  	ready;
  	sc_out<bool> 	reset;

  	sc_in_clk       clock;

    SC_HAS_PROCESS(xtea_RTL_testbench);
    xtea_RTL_testbench (sc_module_name name);
};
