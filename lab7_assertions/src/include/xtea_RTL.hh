#pragma once

//systemc include
#include "systemc.h"

SC_MODULE(xtea_RTL){

    sc_in<sc_uint<32> >  word0;
    sc_in<sc_uint<32> >  word1;
    sc_in<sc_uint<32> >  key0;
    sc_in<sc_uint<32> >  key1;
    sc_in<sc_uint<32> >  key2;
    sc_in<sc_uint<32> >  key3;
    sc_in<bool >  		 ready;
    sc_in<bool>   		 mode;

    sc_out<bool >        done;
    sc_out<sc_uint<32> > res0;
    sc_out<sc_uint<32> > res1;

    sc_in<bool>          reset;
    sc_in_clk			 clk;

    typedef enum { ST_0, ST_1, ST_2, ST_3, ST_4, ST_5, ST_6, ST_7, ST_8, ST_9, ST_10, ST_11, ST_12, ST_13, ST_14} STATES;

    sc_signal<STATES>        STATUS, NEXT_STATUS;
    sc_signal<sc_uint<6> >   Counter;
    sc_uint<64>   Sum;
    sc_uint<32>   V0;
    sc_uint<32>   V1;

    sc_uint<32>   k0;
    sc_uint<32>   k1;
    sc_uint<32>   k2;
    sc_uint<32>   k3;
    sc_uint<32>   Temp;
    sc_uint<32>   Delta_mio;


    //Propriety
    void firstPropriety(void);
    void secondPropriety(void);
    void thirdPropriety(void);

    void elaborate_XTEA(void);
    void elaborate_XTEA_FSM(void);

    SC_CTOR(xtea_RTL){
        SC_METHOD(elaborate_XTEA);
        sensitive << reset.neg();
        sensitive << clk.pos();

        SC_METHOD(elaborate_XTEA_FSM);
        sensitive << STATUS << ready << Counter;

        //Elaborate propriety check
        SC_THREAD(firstPropriety);
            sensitive << clk.pos();

            SC_THREAD(secondPropriety);
            sensitive << clk.pos();

            SC_THREAD(thirdPropriety);
            sensitive << clk.pos();
    };
};

