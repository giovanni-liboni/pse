#ifndef ISTANT_DERIVATE_HH
#define ISTANT_DERIVATE_HH

#include "systemc.h"
#include "systemc-ams.h"

SCA_TDF_MODULE(instant_derivate)
{
    sca_tdf::sca_in<double> in;
    sca_tdf::sca_out<double> out;

    SCA_CTOR(instant_derivate); 

    void set_attributes();

    void processing();

};


#endif
