#ifndef THERMOSTAT_HH
#define THERMOSTAT_HH

#include "systemc.h"
#include "systemc-ams.h"

#include "instant_derivate.hh"
#include "average.hh"
#include "controller.hh"

SC_MODULE( thermostat )
{
    public:
        sca_tdf::sca_in< double > in;
        sca_tdf::sca_out< double > out;

        average avg;
        instant_derivate dx;
        controller ctl;

        thermostat( sc_core::sc_module_name );

    private:
        sca_tdf::sca_signal< double > avg_T;
        sca_tdf::sca_signal< double > dx_T;  

};

#endif
