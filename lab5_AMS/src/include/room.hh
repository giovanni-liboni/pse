#ifndef ROOM_HH
#define ROOM_HH

#define C1 .0001
#define C2 .05
#define T0 17.0

#include <systemc-ams.h>

class room : public sc_core::sc_module
{
  public:
    //--------------------Ports for TDF-----------------------------
    /* TDF Input Ports. */
    sca_tdf::sca_in<double> in_i;
    sca_tdf::sca_in<double> in_v;
    sca_lsf::sca_tdf::sca_source tdf2lsf;

    /* TDF Output Ports. */
    sca_lsf::sca_tdf::sca_sink lsf2tdf;
    sca_tdf::sca_out<double> out;

    SC_CTOR(room);
    void set_attributes();

  private:
    sca_lsf::sca_sub sub1;
    sca_lsf::sca_add add1;
    sca_lsf::sca_dot dot1;
    sca_lsf::sca_gain gain_c1;
    sca_lsf::sca_gain gain_c2;
    sca_lsf::sca_tdf::sca_gain gain_i;

    sca_lsf::sca_source env_temp;
    sca_lsf::sca_signal sig1, sig2, sig3, sig4, sig5, sig6, sig7, sig8;

};

#endif
