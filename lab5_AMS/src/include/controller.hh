#ifndef CONTROLLER_HH
#define CONTROLLER_HH

#include "systemc.h"
#include "systemc-ams.h"

#define TF 20

SCA_TDF_MODULE(controller)
{
    sca_tdf::sca_in<double> in_avg;
    sca_tdf::sca_in<double> in_dx;
    sca_tdf::sca_out<double> out;

    SCA_CTOR(controller);

    void set_attributes();

    void processing();

};


#endif
