#ifndef AVERAGE_HH
#define AVERAGE_HH

#include "systemc.h"
#include "systemc-ams.h"

SCA_TDF_MODULE(average)
{
    sca_tdf::sca_in<double> in;
    sca_tdf::sca_out<double> out;

    SCA_CTOR(average);

    void set_attributes();

    void processing();

};


#endif
