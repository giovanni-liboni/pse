#ifndef HEATER_HH
#define HEATER_HH

#include <systemc-ams>

class heater : public sc_core::sc_module
{
  public:

    sca_tdf::sca_in <double> in;
    sca_eln::sca_tdf::sca_isource i_in;

    sca_tdf::sca_out <double> out;
    sca_eln::sca_tdf::sca_vsink v_out;

    heater(sc_core::sc_module_name _name);
    ~heater();

  private:
    sca_eln::sca_r r1;
    sca_eln::sca_r r2;
    sca_eln::sca_node n1, n2;
    sca_eln::sca_node_ref gnd;
};

#endif
