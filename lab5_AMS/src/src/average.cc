#include "average.hh"


average::average(sc_core::sc_module_name):    
    in("in"),
    out("out") 
{
    // ntd
}

void average::set_attributes()
{
    in.set_rate(2);
    out.set_rate(1);
	out.set_timestep(20, sc_core::SC_MS);

}

void average::processing()
{
	out.write((in.read(0) + in.read(1))/2);

}
