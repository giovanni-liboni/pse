#include "thermostat.hh"
#include "heater.hh"
#include "room.hh"

#include <systemc.h>

int sc_main( int argc, char * argv[] )
{
    sc_core::sc_set_time_resolution( 1, sc_core::SC_FS );

    sca_tdf::sca_signal <double> corrente;
    sca_tdf::sca_signal <double> tensione;
    sca_tdf::sca_signal <double> temperatura;

    sc_core::sc_signal <sc_logic> clk;

    thermostat ts("ts");
    ts.in(temperatura);
    ts.out(corrente);

    heater hea("hea");
    hea.in(corrente);
    hea.out(tensione);

    room ro("ro");
    ro.in_i(corrente);
    ro.in_v(tensione);
    ro.out(temperatura);

    sca_util::sca_trace_file* atf =
        sca_util::sca_create_vcd_trace_file( "tr.vcd" );

    sca_util::sca_trace( atf, corrente, "corrente" );
    sca_util::sca_trace( atf, tensione, "tensione" );
    sca_util::sca_trace( atf, temperatura, "temperatura" );


    sc_core::sc_start( 180, sc_core::SC_SEC );
    sca_util::sca_close_vcd_trace_file( atf );

    return 0;
}
