#include "thermostat.hh"

thermostat::thermostat( sc_core::sc_module_name _name ) :
    sc_core::sc_module( _name ),
    in("in"),
    out("out"),
    avg("avg"),
    dx("dx"),
    ctl("ctl")
{
    avg.in(in);
    dx.in(in);
    avg.out(avg_T);
    dx.out(dx_T);
    ctl.in_avg(avg_T);
    ctl.in_dx(dx_T);
    ctl.out(out);
}

