#include "instant_derivate.hh"

instant_derivate::instant_derivate(sc_module_name name) :
    in("in"),
    out("out")
{
    // ntd
}

void instant_derivate::set_attributes()
{
    in.set_rate(2);
    out.set_rate(1);
}

void instant_derivate::processing()
{
	out.write(in.read(1) - in.read(0));
}
