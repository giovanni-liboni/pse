#include "controller.hh"


controller::controller(sc_core::sc_module_name):
    in_avg("in_avg"),
    in_dx("in_dx"),
    out("out")
{
    // ntd
} 

void controller::set_attributes()
{
    out.set_rate(2);
    out.set_delay(2);
}

void controller::processing()
{
    if((in_avg.read() < (TF + .5) && in_dx.read() >= 0) || in_avg.read() <= (TF - .5))
    {
	    out.write(10.0, 0);
        out.write(10.0, 1);
    }   

    else //if ((in_avg.read() > (TF - .5) && in_dx.read() <= 0) || in_avg.read() >= (TF + .5))
    {
        out.write(0.0, 0);
        out.write(0.0, 1);
    }
}
