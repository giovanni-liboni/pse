#include "room.hh"

room::room( sc_core::sc_module_name _name ) :
    sc_core::sc_module( _name ),
    in_i("in_i"),
    in_v("in_v"),
    tdf2lsf("tdf2lsf"),
    lsf2tdf("lsf2tdf"),
    out("out"),
    sub1("sub1"),
    add1("add1"),
    dot1("dot1"),
    gain_c1("gain_c1", C1),
    gain_c2("gain_c2", 1.0/C2),
    gain_i("gain_i"),
    env_temp("env_temp", T0, T0)
{

    /* TDF Input Ports. */
    tdf2lsf.inp(in_v);
    tdf2lsf.y(sig1);

    gain_i.x(sig1);
    gain_i.y(sig2);
    gain_i.inp(in_i);

    gain_c1.x(sig2);
    gain_c1.y(sig3);

    sub1.x1(sig3);
    sub1.x2(sig6);
    sub1.y(sig4);

    gain_c2.x(sig4);
    gain_c2.y(sig5);

    dot1.x(sig5);
    dot1.y(sig6);
    
    env_temp.y(sig8);

    add1.x1(sig5);
    add1.x2(sig8);
    add1.y(sig7); 


    /* TDF Output Ports. */
    lsf2tdf.x(sig7);
    lsf2tdf.outp(out);

}

void room::set_attributes()
{
	out.set_timestep(10, sc_core::SC_MS);
}
