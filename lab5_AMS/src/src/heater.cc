#include "heater.hh"

#define R1 7
#define R2 14

heater::heater(sc_core::sc_module_name _name):
    sc_core::sc_module( _name ),
    in("in"),
    i_in("i_in"),
    out("out"),
    v_out("v_out"),
    r1("r1", R1),
    r2("r2", R2),
    gnd("gnd")
{
    i_in.inp(in);
    i_in.p(gnd);
    i_in.n(n1);

    r1.p(n1);
    r1.n(n2);

    r2.p(n2);
    r2.n(gnd);

    v_out.p(n1);
    v_out.n(gnd);
    v_out.outp(out);
}

heater::~heater()
{
    // ntd
}
