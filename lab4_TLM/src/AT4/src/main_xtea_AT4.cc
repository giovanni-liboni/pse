#include "xtea_AT4.hh"
#include "xtea_AT4_testbench.hh"

/* Libraries */
#include "xtea_AT4.hh"
#include "xtea_AT4_testbench.hh"

class xtea_top : public sc_module{

private:

    xtea target;

    testBench initiator;


public:

    xtea_top(sc_module_name name) : sc_module(name), target("target"), initiator("initiator")  {

        //Binding delle porte
        initiator.socket(target.socket);
    }

};

//Main del sistema
int main(int argc, char* argv[]){

    xtea_top top("top");
    sc_start();

    return 0;

}