# Restart the simulation (and ignore confirmation message).
restart -f

# Open the Wave pane (undocked) or eventually clean it.
# delete wave *
view wave 
view list
#-undock

# Select all primary inputs, primary output and internal signals and
# show them in the Wave pane.
add wave *

# Run the simulation for 50 ns.
run 50

# Show Delta behavior.
add list *

