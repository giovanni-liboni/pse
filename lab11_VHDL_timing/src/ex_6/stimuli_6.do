# Restart the simulation (and ignore confirmation message).
restart -f

# Open the Wave pane (undocked).
view wave -undock

# Select all primary inputs, primary output and internal signals and
# show them in the Wave pane.
add wave *

force A 0 0
force B 0 0
force C 0 0
force D 0 0
force E 0 0

force -deposit A 0 10
force -deposit B 0 10
force -deposit C 0 10
force -deposit D 0 10

force -deposit E 1 10


# Run the simulation for 1000ns.
run 1000 ns