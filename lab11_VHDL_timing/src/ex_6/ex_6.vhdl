
entity ex_6 is
  port ( 
         A,B,C,D : inout integer;
		 E: inout bit
       );
end ex_6;


architecture behav of ex_6 is

begin

  P1: process
  begin
	wait on E;
	A <= 1 after 5 ns;
	B <= A +1;
	C <= B after 10 ns;
	wait for 0 ns;
	D <= B after 3 ns;
	A <= A + 5 after 15 ns;
	B <= B + 7;
  end process P1;

end architecture behav;