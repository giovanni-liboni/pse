# Restart the simulation (and ignore confirmation message).
restart -f

# Open the Wave pane (undocked).
view wave 
delete wave *
#-undock

# Select all primary inputs, primary output and internal signals and
# show them in the Wave pane.
add wave *


# Setup the clock signal behavior.
# @0ns   clk = 0
# @50ns  clk = 1
# @100ns clk = 0
# @150ns clk = 1
# ...
# N.B. -r 100 stays for "repeat this behavior every 100 time units"
force clk 0 0, 1 50 -r 100

# x changes its value on the clock rising edge.
force x 0 0
force x 1 15
force x 0 54
force x 1 60
force x 0 75

force x 1 202
force x 0 225
force x 1 240
force x 0 245

force x 1 430
force x 0 455

force x 1 675
force x 0 710

# Run the simulation for 1000ns.
run 1000

# Turn-off the simulator.
quit -sim
