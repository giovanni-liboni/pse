# Restart the simulation (and ignore confirmation message).
restart -f

# Open the Wave pane (undocked).
view wave -undock

# Select all primary inputs, primary output and internal signals and
# show them in the Wave pane.
add wave *

# Setup the clock signal behavior.
# @0ns   clk = 0
# @50ns  clk = 1
# @100ns clk = 0
# @150ns clk = 1
# ...
# N.B. -r 100 stays for "repeat this behavior every 100 time units"
force clk 0 0, 1 50 -r 100

# Force the x signal to 1 @time 14ns.
force x 1 14

# Run the simulation for 1000ns.
run 1000
