# Restart the simulation (and ignore confirmation message).
restart -f

# Open the Wave pane (undocked).
view wave
delete wave *
#-undock

# Select all primary inputs, primary output and internal signals and
# show them in the Wave pane.
add wave *

# E changes its value at t = 5 ns
force D 0 0
force -deposit D 1 5

# Run the simulation for 200 ns.
delete list *
run 200
add list *
