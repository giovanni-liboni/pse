#include "xtea_RTL_transactor.hh"

/* Libraries */
#include "transactors.h"

transactors :: transactors(sc_module_name name) : sc_module(name), socket("target_socket"){
    socket(*this);
    //Creo la thread per il writeProcess sensibile al segnale di clock
    SC_THREAD(WRITEPROCESS);
    sensitive_pos << clk;
    //Creo la thread per il readProcess sensibile al segnale di clock
    SC_THREAD(READPROCESS);
    sensitive_pos << clk;
}

void transactors::b_transport(tlm::tlm_generic_payload& trans, sc_time& t){
    cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> Start b_transport" << "\033[0m" << "\n";
    wait(0, SC_NS);
    tlm::tlm_command trans_command = trans.get_command();

    switch (trans_command) {

        case tlm::TLM_WRITE_COMMAND:
            cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> TLM_WRITE_COMMAND" << "\033[0m" << "\n";
            xteaData = *((structxtea*) trans.get_data_ptr());
            trans.set_response_status(tlm::TLM_OK_RESPONSE);
            begin_write.notify();
            cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> Before WAIT(end_write)" << "\033[0m" << "\n";
            wait(end_write);
            cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> After WAIT(end_write)" << "\033[0m" << "\n";
            break;

        case tlm::TLM_READ_COMMAND:
            cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> TLM_READ_COMMAND" << "\033[0m" << "\n";
            xteaData = *((structxtea*) trans.get_data_ptr());
            trans.set_response_status(tlm::TLM_OK_RESPONSE);
            begin_read.notify();
            cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> Before WAIT(end_read)" << "\033[0m" << "\n";
            wait(end_read);
            cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> After WAIT(end_read)" << "\033[0m" << "\n";
            *((structxtea*) trans.get_data_ptr()) = xteaData;
            break;

        default:
            break;

    }
    cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> End b_transport" << "\033[0m" << "\n";
}

//Terminated elaboration, reset signals
void transactors :: end_of_elaboration(){
    cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> Start end_of_elaboration" << "\033[0m" << "\n";
    reset();
    cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> End end_of_elaboration" << "\033[0m" << "\n";
}

//Reset signals
void transactors :: reset(){
    cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> Start reset signal" << "\033[0m" << "\n";
    s_reset.write(0);
    ready.write(0);
    word0.write(0);
    word1.write(0);
    key0.write(0);
    key1.write(0);
    key2.write(0);
    key3.write(0);
    cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> End reset signal" << "\033[0m" << "\n";
}

//Other TLM functions
bool transactors::get_direct_mem_ptr(tlm::tlm_generic_payload& trans, tlm::tlm_dmi& dmi_data) {
    return false;
}

tlm::tlm_sync_enum transactors::nb_transport_fw(tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& t) {
    return tlm::TLM_UPDATED;
}

unsigned int transactors::transport_dbg(tlm::tlm_generic_payload& trans) {
    return 0;
}

//Write process implementation
void transactors::WRITEPROCESS() {

    while (true) {
        wait(begin_write);
        cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> Start WRITEPROCESS" << "\033[0m" << "\n";
        s_reset.write(1);
        mode.write(xteaData.mode);
        word0.write(xteaData.word0);
        word1.write(xteaData.word1);
        key0.write(xteaData.key0);
        key1.write(xteaData.key1);
        key2.write(xteaData.key2);
        key3.write(xteaData.key3);
        ready.write(1);
        end_write.notify();
        cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> End WRITEPROCESS" << "\033[0m" << "\n";
        wait();
    }
}

//Read process implementation
void transactors::READPROCESS() {
    while (true) {
        wait(begin_read);
        cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> Start READPROCESS" << "\033[0m" << "\n";
        cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> Waiting done == 1" << "\033[0m" << "\n";
        while(done.read() != 1){
            wait();
        }
        cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> Now done = 1" << "\033[0m" << "\n";
        xteaData.result0=result0.read();
        xteaData.result1=result1.read();
        cout << "\033[32m" << "   transactors("<<sc_simulation_time()<<") -> End READPROCESS" << "\033[0m" << "\n";
        end_read.notify();
    }
}
