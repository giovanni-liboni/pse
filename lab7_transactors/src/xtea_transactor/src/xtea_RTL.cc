#include "xtea_RTL.hh"

/*
* Implementazione del datapath
*/
void xtea_RTL::elaborate_XTEA(){

      if (reset.read() == 0){
        STATUS = ST_0;
      }
      else if (clk.read() == 1) {
        STATUS = NEXT_STATUS;
        switch(STATUS) {
            case ST_0:
            	// Resetto i segnali interni
                Counter.write(0);
                Sum = 0;
                Temp = 0;
                // Resetto le uscite
                res0.write(0);
                res1.write(0);
                done.write(0);
                break;
            case ST_1:
            	// Leggo i valori di input
            	k0 = key0.read();
            	k1 = key1.read();
            	k2 = key2.read();
            	k3 = key3.read();

            	V0 = word0.read();
            	V1 = word1.read();
                break;
            case ST_2:
                Delta_mio = 0x9e3779b9;
                break;
            case ST_3:
                Sum += Delta_mio;
                break;
            case ST_4:
                if((Sum & 3) == 0)
                    Temp = k0;
                else if((Sum & 3) == 1)
                    Temp = k1;
                else if ((Sum & 3) == 2)
                    Temp = k2;
                else Temp = k3;
                break;
            case ST_5:
                V0 += ((((V1<<4) ^ (V1>>5)) + V1) ^ (Sum + Temp) );
                break;
            case ST_6:
                if(((Sum>>11) & 3) == 0)
                    Temp = k0;
                else if(((Sum>>11) & 3) == 1)
                    Temp = k1;
                else if (((Sum>>11) & 3) == 2)
                    Temp = k2;
                else Temp = k3;
                break;

            case ST_7:
                V1 += ((((V0<<4) ^ (V0>>5)) + V0) ^ (Sum + Temp) );
                Counter.write(Counter.read() + 1);
                break;
            case ST_8:
                Delta_mio = 0x9e3779b9;
                Sum = 0x13c6ef3720;
                break;
            case ST_9:
                if(((Sum>>11) & 3) == 0)
                    Temp = k0;
                else if(((Sum>>11) & 3) == 1)
                    Temp = k1;
                else if (((Sum>>11) & 3) == 2)
                    Temp = k2;
                else Temp = k3;
                break;
            case ST_10:
                V1 -= ((((V0<<4) ^ (V0>>5)) + V0) ^ (Sum + Temp) );
                break;
            case ST_11:
                Sum -= Delta_mio;
                break;
            case ST_12:
                if((Sum & 3) == 0)
                    Temp = k0;
                else if((Sum & 3) == 1)
                    Temp = k1;
                else if ((Sum & 3) == 2)
                    Temp = k2;
                else Temp = k3;
                break;
            case ST_13:
                V0 -= ((((V1<<4) ^ (V1>>5)) + V1) ^ (Sum + Temp) );
                Counter.write(Counter.read() + 1);
                break;
            case ST_14:
                res0.write(V0);
                res1.write(V1);
                done.write(1);
                break;
            default:
                break;
        }
      }
}

/*
* Funzione per gestire la FSM
*/
void xtea_RTL::elaborate_XTEA_FSM(){
	NEXT_STATUS = STATUS;
    switch (STATUS) {
        case ST_0:
            if (ready.read() == 1) {
                NEXT_STATUS = ST_1;
            } else {
                NEXT_STATUS = ST_0;
            }
            break;
        case ST_1:
            if (mode.read() == 1) {
                NEXT_STATUS = ST_8;
            } else {
                NEXT_STATUS = ST_2;
            }
            break;
        case ST_2:
            NEXT_STATUS = ST_3;
            break;
        case ST_3:
            NEXT_STATUS = ST_4;
            break;
        case ST_4:
            NEXT_STATUS = ST_5;
            break;
        case ST_5:
            NEXT_STATUS = ST_6;
            break;
        case ST_6:
            NEXT_STATUS = ST_7;
            break;
        case ST_7:
            if (Counter.read() < 31) {
                NEXT_STATUS = ST_3;
            } else {
                NEXT_STATUS = ST_14;
            }
            break;
        case ST_8:
            NEXT_STATUS = ST_9;
            break;
        case ST_9:
            NEXT_STATUS = ST_10;
            break;
        case ST_10:
            NEXT_STATUS = ST_11;
            break;
        case ST_11:
            NEXT_STATUS = ST_12;
            break;
        case ST_12:
        NEXT_STATUS = ST_13;
            break;
        case ST_13:
            if (Counter.read() < 31) {
                NEXT_STATUS = ST_9;
            } else {
                NEXT_STATUS = ST_14;
            }
            break;

        case ST_14:
            NEXT_STATUS = ST_0;
            break;
    }
}
