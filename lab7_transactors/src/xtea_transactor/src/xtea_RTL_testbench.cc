#include "xtea_RTL_testbench.hh"

xtea_RTL_testbench::xtea_RTL_testbench(sc_module_name name_)
  : sc_module(name_)
{
  SC_THREAD(run);
	sensitive << clock.pos();
}

void
xtea_RTL_testbench::run()
{
	sc_uint<32> r0, r1;

    uint32_t w0, w1, k0, k1, k2, k3;

    cout << "Encryption" << endl;


    w0 = 0x12345678;
    w1 = 0x9abcdeff;
    k0 = 0x6a1d78c8;
    k1 = 0x8c86d67f;
    k2 = 0x2a65bfbe;
    k3 = 0xb4bd6e46;

    reset.write(1);

	mode.write(0);

    word0.write(w0);
    word1.write(w1);

    key0.write(k0);
    key1.write(k1);
    key2.write(k2);
    key3.write(k3);
    ready.write(1);

    wait();

    cout << " Word    : " << hex << word0.read() << word1.read() << endl;
    cout << " Key     : " << hex << key0.read() << key1.read() << key2.read() << key3.read() << endl;

	while(done.read() != 1){
		wait();
	}

	r0 = res0.read();
	r1 = res1.read();

    cout << " Results : " << hex << res0.read() << res1.read() << endl;

    if (r0 != 0x8689372f || r1 != 0x6a7a8bcf) {
		cout << "Failed encryption" << endl;
	} else {
        cout << "OK" << endl;
    }

    reset.write(0);
    wait();
    cout << "Decryption" << endl;

    reset.write(1);
    mode.write(1);

    w0 = 0x12345678;
    w1 = 0x9abcdeff;

    word0.write(w0);
    word1.write(w1);

    k0 = 0x62ee209f;
    k1 = 0x69b7afce;
    k2 = 0x376a8936;
    k3 = 0xcdc9e923;

    key0.write(k0);
    key1.write(k1);
    key2.write(k2);
    key3.write(k3);
    ready.write(1);

    wait();
    cout << " Word    : " << hex << word0.read() << word1.read() << endl;
    cout << " Key     : " << hex << key0.read() << key1.read() << key2.read() << key3.read() << endl;


    while(done.read() != 1){
        wait();
    }

    r0 = res0.read();
    r1 = res1.read();

    cout << " Results : " << hex << res0.read() << res1.read() << endl;

    if (r0 != 0x1f22e82b || r1 != 0xa0175b44) {
        cout << "Failed decryption" << endl;
    } else {
        cout << "OK" << endl;
    }


    sc_stop();

}
