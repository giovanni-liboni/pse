#pragma once

#include <systemc-ams>

class heater : public sc_core::sc_module
{
  public:

    sca_tdf::sca_in <double> in;
    sca_eln::sca_tdf::sca_isource current_source;

    sca_tdf::sca_out <double> out;
    sca_eln::sca_tdf::sca_vsink voltage_out_tdf;

    heater (sc_core::sc_module_name _name);

  private:
    sca_eln::sca_r r1;
    sca_eln::sca_r r2;
    sca_eln::sca_node n1, n2;
    sca_eln::sca_node_ref gnd;
};

