#pragma once

#include <systemc>
#include <systemc-ams>

SCA_TDF_MODULE(average_tmp)
{
    sca_tdf::sca_in<double> in;
    sca_tdf::sca_out<double> out;

    SCA_CTOR(average_tmp);

    void set_attributes();
    void processing();
};
