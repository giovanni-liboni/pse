#pragma once

#include <systemc-ams>

class room : public sc_core::sc_module
{
  public:
    sca_tdf::sca_in<double> in_i;
    sca_tdf::sca_in<double> in_v;
    sca_lsf::sca_tdf::sca_source tdf2lsf;
    sca_lsf::sca_tdf::sca_sink lsf2tdf;
    sca_tdf::sca_out<double> out;

    SC_CTOR(room);

  private:
    sca_lsf::sca_sub _sub;
    sca_lsf::sca_add _add;
    sca_lsf::sca_dot _dot;

    sca_lsf::sca_gain _gainC1;
    sca_lsf::sca_gain _gainC2;

    sca_lsf::sca_tdf_gain _gain_i;

    sca_lsf::sca_source _room_tmp;
    sca_lsf::sca_signal _s1, _s2, _s3, _s4, _s5, _s6, _s7, _s8;

};
