#pragma once

#include <systemc-ams.h>
#include <systemc.h>
#include "room.hh"
#include "RoomIFace.hh"
#include "heater.hh"
#include "HeaterIFace.hh"
#include "PlantTransactor.hh"
#include "thermostat.hh"
#include "ThermostatTransactor.hh"
#include "ThermostatIFace.hh"

class System : public sc_core::sc_module
{
public:
    System( sc_core::sc_module_name );
    ~System();
    sca_tdf::sca_signal< double > temp, voltage, current;


protected:
    room r;
    room_iface ri;
    heater h;
    heater_iface hi;
    PlantTransactor pt;

    thermostat t;
    thermostat_iface ti;
    ThermostatTransactor tt;

    sca_tdf::sca_signal< double > sig_r_tdf_bw, sig_h_tdf_bw;
    sc_core::sc_signal< double > sig_tt_de_bw, sig_pt_de_bw;

};