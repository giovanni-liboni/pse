#pragma once

#include <systemc-ams>
#include "instant_derivate_tmp.hh"
#include "average_tmp.hh"
#include "logic.hh"

SC_MODULE( thermostat )
{
    public:
        sca_tdf::sca_in< double > in;
        sca_tdf::sca_out< double > out;

        logic logicModule;
        average_tmp avg_temp;
        instant_derivate_tmp id_temp;

        thermostat( sc_core::sc_module_name );

    private:
        sca_tdf::sca_signal< double > avg_T;
        sca_tdf::sca_signal< double > dx_T;

};

