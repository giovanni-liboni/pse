#pragma once

#include <systemc-ams>

SCA_TDF_MODULE( heater_iface )
{
public:
    sca_tdf::sca_out< double > outc;

    sca_tdf::sca_de::sca_in< double > inp;

    heater_iface( sc_core::sc_module_name );

    virtual ~heater_iface();

    void processing();
};