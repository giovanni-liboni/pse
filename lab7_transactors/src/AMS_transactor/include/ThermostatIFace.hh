#pragma once

#include <systemc-ams>

SCA_TDF_MODULE( thermostat_iface )
{
    public:
    sca_tdf::sca_in< double > inc;

    sca_tdf::sca_de::sca_out< double > outp;

    thermostat_iface( sc_core::sc_module_name );

    ~thermostat_iface();

    void processing();
};