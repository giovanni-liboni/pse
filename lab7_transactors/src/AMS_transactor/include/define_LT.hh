#pragma once

#include <systemc>

struct iostruct
{
    double value_fw;
    double value_bw;
};