#pragma once

#include <systemc-ams>

SCA_TDF_MODULE( room_iface )
{
public:
    sca_tdf::sca_out< double > outc_i;

    sca_tdf::sca_de::sca_in< double > inp_i;

    room_iface( sc_core::sc_module_name );

    ~room_iface();

    void processing();
};