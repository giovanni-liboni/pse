#include "HeaterIFace.hh"

heater_iface::heater_iface(sc_core::sc_module_name) :
        outc( "outc" ),
        inp( "inp" )
{
}
heater_iface::~heater_iface()
{
}

void heater_iface::processing()
{
    outc.write( inp.read() );
}