#include "logic.hh"


logic::logic(sc_core::sc_module_name):
    in_avg("in_avg"),
    in_id("in_id"),
    out("out")
{

}

void logic::set_attributes()
{
    out.set_delay(2);
    out.set_rate(2);
}

void logic::processing()
{
    if ((in_avg.read() <= (DEFAULT + 0.5) && in_id.read() > 0) || in_avg.read() < (DEFAULT))
    {
	    out.write(12, 0);
        out.write(12, 1);
    }
    else
    {
        out.write(0.0, 0);
        out.write(0.0, 1);
    }
}
