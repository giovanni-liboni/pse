#include "System.hh"


System::System( sc_core::sc_module_name ) :
        r("room"),
        ri("room_iface"),
        h("heater"),
        hi("heater_iface"),
        pt("PlantTransactor"),
        t("thermostat"),
        tt("ThermostatTransactor"),
        ti("thermostat_iface")
{
    // Thermostat Interface Side
    t.out(current);
    ti.inc(current);

    // Thermostat Interface Transactor Side
    ti.outp(sig_tt_de_bw);
    tt.value_bw(sig_tt_de_bw);

    // Transactors Connection
    tt.initiator_socket(pt.target_socket);

    // Plant Transactor Side
    pt.value_bw(sig_pt_de_bw);
    hi.inp(sig_pt_de_bw);
    ri.inp_i(sig_pt_de_bw);


    // Heater Interface Side
    hi.outc(sig_h_tdf_bw);
    h.in(sig_h_tdf_bw);

    // Room Interface Side
    ri.outc_i(sig_r_tdf_bw);
    r.in_i(sig_r_tdf_bw);

    // Room -> Thermostat (Temperature)
    r.out(temp);
    t.in(temp);

    // Heater -> Room (Voltage)
    h.out(voltage);
    r.in_v(voltage);
}

System::~System()
{
}
