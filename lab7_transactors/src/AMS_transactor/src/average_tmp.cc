#include "average_tmp.hh"


average_tmp::average_tmp(sc_core::sc_module_name):
    in("in"),
    out("out") 
{

}

void average_tmp::set_attributes()
{
    out.set_rate(1);
    out.set_timestep(50, sc_core::SC_MS);

    in.set_rate(2);
}

void average_tmp::processing()
{
	out.write((in.read(0) + in.read(1)) / 2);

}
