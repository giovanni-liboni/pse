#include "System.hh"

int main()
{
    System system( "system" );

    sca_util::sca_trace_file* atf = sca_util::sca_create_vcd_trace_file( "tr.vcd" );

    sca_util::sca_trace( atf, system.temp, "Temperature" );
    sca_util::sca_trace( atf, system.current, "Current" );
    sca_util::sca_trace( atf, system.voltage, "Voltage" );

    sc_start( 360, SC_SEC );
    sca_util::sca_close_vcd_trace_file( atf );
}