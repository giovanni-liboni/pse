#include "ThermostatIFace.hh"

thermostat_iface::thermostat_iface(sc_core::sc_module_name) :
        inc( "inc" ),
        outp( "outp" )
{
}
thermostat_iface::~thermostat_iface()
{
}

void thermostat_iface::processing()
{
    outp.write( inc.read() );
}
