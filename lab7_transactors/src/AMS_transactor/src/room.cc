#include "room.hh"

room::room( sc_core::sc_module_name) :
        in_i("in_i"),
        in_v("in_v"),
        tdf2lsf("tdf2lsf"),
        lsf2tdf("lsf2tdf"),
        out("out"),
        _sub("_sub"),
        _add("_add"),
        _dot("_dot"),
        _gainC1("_gainC1", .0001),
        _gainC2("_gainC2", 1.0/.02),
        _gain_i("_gain_i"),
        _room_tmp("_room_tmp", 20, 20)
{

    tdf2lsf.inp(in_v);
    tdf2lsf.y(_s1);

    _gain_i.x(_s1);
    _gain_i.y(_s2);
    _gain_i.inp(in_i);

    _gainC1.x(_s2);
    _gainC1.y(_s3);

    _sub.x1(_s3);
    _sub.x2(_s6);
    _sub.y(_s4);

    _gainC2.x(_s4);
    _gainC2.y(_s5);

    _dot.x(_s5);
    _dot.y(_s6);

    _room_tmp.y(_s8);

    _add.x1(_s5);
    _add.x2(_s8);
    _add.y(_s7);

    lsf2tdf.x(_s7);
    lsf2tdf.outp(out);

    lsf2tdf.set_timestep(25, sc_core::SC_MS);

}