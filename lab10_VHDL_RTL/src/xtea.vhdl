-- xtea VHDL 
PACKAGE xtea_pack IS
    CONSTANT SIZE : INTEGER := 32;
	
    CONSTANT ST_0 : INTEGER := 0;
    CONSTANT ST_1 : INTEGER := 1;
    CONSTANT ST_2 : INTEGER := 2;
    CONSTANT ST_3 : INTEGER := 3;
    CONSTANT ST_4 : INTEGER := 4;
    CONSTANT ST_5 : INTEGER := 5;
    CONSTANT ST_6 : INTEGER := 6;
    CONSTANT ST_7 : INTEGER := 7;
    CONSTANT ST_8 : INTEGER := 8;
    CONSTANT ST_9 : INTEGER := 9;
    CONSTANT ST_10 : INTEGER := 10;
    CONSTANT ST_11 : INTEGER := 11;
    CONSTANT ST_12 : INTEGER := 12;
    CONSTANT ST_13 : INTEGER := 13;
    CONSTANT ST_14 : INTEGER := 14;
END xtea_pack;

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE WORK.xtea_pack.ALL;
USE IEEE.NUMERIC_BIT.ALL;

entity xtea is
	port (	
	      clock, reset : in  bit;
        word0   : in  UNSIGNED (SIZE-1 DOWNTO 0);
        word1   : in  UNSIGNED (SIZE-1 DOWNTO 0);
        key0    : in  UNSIGNED (SIZE-1 DOWNTO 0);
        key1    : in  UNSIGNED (SIZE-1 DOWNTO 0);
        key2    : in  UNSIGNED (SIZE-1 DOWNTO 0);
        key3    : in  UNSIGNED (SIZE-1 DOWNTO 0);
        ready   : in  bit;
        mode    : in  bit;
        res0    : out UNSIGNED (SIZE-1 DOWNTO 0);
        res1    : out UNSIGNED (SIZE-1 DOWNTO 0);
        done    : out  bit
	);
end xtea;

architecture rtl of xtea is
  -- Type of internal signals
	subtype status_t is INTEGER range 0 to 14;
	subtype internal_t is UNSIGNED (SIZE-1 DOWNTO 0);
	-- Declaration internal signals
  signal counter: integer range 0 to 32;	
	signal STATUS: status_t;
	signal NEXT_STATUS: status_t;
	-- Assignment internal constants
  CONSTANT ZERO     : internal_t := "00000000000000000000000000000000";
  CONSTANT CONST_31 : internal_t := "00000000000000000000000000011111";
begin

process(STATUS, ready, counter, mode) -- Sensitivity list
	begin
		case STATUS is
		  when ST_0 => 
            if ready = '1' then
                NEXT_STATUS <= ST_1;
            else
                NEXT_STATUS <= ST_0;
            end if;
        when ST_1 =>
            if mode = '1' then
                NEXT_STATUS <= ST_8;
            else
                NEXT_STATUS <= ST_2;
            end if;

        when ST_2 =>
            NEXT_STATUS <= ST_3;
            
        when ST_3 =>
            NEXT_STATUS <= ST_4;
            
        when ST_4 =>
            NEXT_STATUS <= ST_5;
            
        when ST_5 =>
            NEXT_STATUS <= ST_6;
            
        when ST_6 =>
            NEXT_STATUS <= ST_7;
            
        when ST_7 =>
            if counter <= CONST_31 then
                NEXT_STATUS <= ST_3;
            else
                NEXT_STATUS <= ST_14;
            end if;

        when ST_8 =>
            NEXT_STATUS <= ST_9;
            
        when ST_9 =>
            NEXT_STATUS <= ST_10;
            
        when ST_10 =>
            NEXT_STATUS <= ST_11;
            
        when ST_11 =>
            NEXT_STATUS <= ST_12;
            
        when ST_12 =>
            NEXT_STATUS <= ST_13;
            
        when ST_13 =>
            if counter <= CONST_31 then 
                NEXT_STATUS <= ST_9;
            else
                NEXT_STATUS <= ST_14;
            end if;

        when ST_14 =>
                NEXT_STATUS <= ST_0;
        
        end case;
	end process;
	
	
process(clock, reset) 
-- elaborate_xtea
    variable sum: internal_t;
    variable v0: internal_t;
    variable v1: internal_t;
    variable k0: internal_t;
    variable k1: internal_t;
    variable k2: internal_t;
    variable k3: internal_t;
    variable delta: internal_t;
    variable temp: internal_t;
begin
	  if reset='0' then 
		   STATUS <= ST_0;
	       res0 <= ZERO;
				 res1 <= ZERO;
         done <= '0';
	  elsif clock'event and clock='1' then 
		  STATUS <= NEXT_STATUS;
		case NEXT_STATUS is
            when ST_0 =>
                res0 <= ZERO;
                res1 <= ZERO;
                counter <= 0;
                sum := ZERO;
                temp := ZERO;
                done <= '0';

			when ST_1 =>
                v0 := word0;
                v1 := word1;
                k0 := key0;
                k1 := key1;
                k2 := key2;
                k3 := key3;
            when ST_2 =>
                delta := "10011110001101110111100110111001";
               
            when ST_3 =>
                sum := sum + delta;
                
            when ST_4 =>
		        case sum (1 downto 0) is
                    when "00" =>
                        temp := k0;
                    when "01" =>
                        temp := k1;
                    when "10" =>
                        temp := k2;
                    when "11" =>
                        temp := k3;
                end case;
                
            when ST_5 =>
                v0 := v0 + ((((v1 sll 4) xor (v1 srl 5)) + v1) xor (sum + temp));
                
            when ST_6 =>
		        case sum (12 downto 11) is
                    when "00" =>
                        temp := k0;
                    when "01" =>
                        temp := k1;
                    when "10" =>
                        temp := k2;
                    when "11" =>
                        temp := k3;
                end case;
                
            when ST_7 =>
                v1 := v1 + ((((v0 sll 4) xor (v0 srl 5)) + v0) xor (sum + temp));
                counter <= counter + 1;
                
-- mode 1
            when ST_8 =>
                sum := "11000110111011110011011100100000";
                delta := "10011110001101110111100110111001";

            when ST_9 =>
		        case sum (12 downto 11) is
                    when "00" =>
                        temp := k0;
                    when "01" =>
                        temp := k1;
                    when "10" =>
                        temp := k2;
                    when "11" =>
                        temp := k3;
                end case;
                
            when ST_10 =>
                v1 := v1 - ((((v0 sll 4) xor (v0 srl 5)) + v0) xor (sum + temp));
                
            when ST_11 =>
                sum := sum - delta;
                
            when ST_12 =>
		        case sum (1 downto 0) is
                    when "00" =>
                        temp := k0;
                    when "01" =>
                        temp := k1;
                    when "10" =>
                        temp := k2;
                    when "11" =>
                        temp := k3;
                end case;
                
            when ST_13 =>
                v0 := v0 - ((((v1 sll 4) xor (v1 srl 5)) + v1) xor (sum + temp));
                counter <= counter + 1;
                
 -- end
            when ST_14 =>
                res0 <= v0;
                res1 <= v1;
                done <= '1';

		  end case;
    end if;        
	end process;
end rtl;


