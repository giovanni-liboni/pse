onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /xtea/clock
add wave -noupdate /xtea/reset
add wave -noupdate /xtea/word0
add wave -noupdate /xtea/word1
add wave -noupdate /xtea/key0
add wave -noupdate /xtea/key1
add wave -noupdate /xtea/key2
add wave -noupdate /xtea/key3
add wave -noupdate /xtea/ready
add wave -noupdate /xtea/mode
add wave -noupdate /xtea/res0
add wave -noupdate /xtea/res1
add wave -noupdate /xtea/done
add wave -noupdate /xtea/counter
add wave -noupdate /xtea/STATUS
add wave -noupdate /xtea/NEXT_STATUS
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {13317 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {12812 ns} {13641 ns}
