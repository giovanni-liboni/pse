add wave *

force clock 1 20 ns, 0 40 ns -repeat 40
force reset 1 0

force mode   0 1
force word0  0x12345678 0
force word1  0x9abcdeff 0
force key0   0x6a1d78c8 0
force key1   0x8c86d67f 0
force key2   0x2a65bfbe 0
force key3   0xb4bd6e46 0
force ready  1 1
force ready  0 1000
run 6800

force mode   1 0
force word0  0x12345678 0
force word1  0x9abcdeff 0
force key0   0x62ee209f 0
force key1   0x69b7afce 0
force key2   0x376a8936 0
force key3   0xcdc9e923 0
force ready  1 1
force ready  0 1000
run 6800

