//
// Created by Giovanni Liboni on 25/10/15.
//

#include "xtea_RTL.hh"

/*
* Implementazione del datapath
*/
void xtea_RTL::elaborate_XTEA(){

      if (reset.read() == 0){
        STATUS = ST_0;
      }
      else if (clk.read() == 1) {
        STATUS = NEXT_STATUS;
        switch(STATUS) {
            case ST_0:
                cout << "\t" << sc_time_stamp() << " - Elaborate: ST_0" <<endl;
            	// Resetto i segnali interni
                Counter.write(0);
                Sum = 0;
                Temp = 0;
                // Resetto le uscite
                res0.write(0);
                res1.write(0);
                done.write(0);
                break;
            case ST_1:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_1" <<endl;
            	// Leggo i valori di input
            	k0 = key0.read();
            	k1 = key1.read();
            	k2 = key2.read();
            	k3 = key3.read();

            	V0 = word0.read();
            	V1 = word1.read();
                break;
             // encipher
            case ST_2:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_2" <<endl;
                Delta_mio = 0x9e3779b9;
                break;
            case ST_3:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_3" <<endl;
                if((Sum & 3) == 0)
                    Temp = k0;
                else if((Sum & 3) == 1)
                    Temp = k1;
                else if ((Sum & 3) == 2)
                    Temp = k2;
                else Temp = k3;
                break;
            case ST_4:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_4" <<endl;
                V0 += ((((V1<<4) ^ (V1>>5)) + V1) ^ (Sum + Temp) );
                break;
            case ST_5:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_5" <<endl;
                Sum += Delta_mio;
                break;
            case ST_6:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_6" <<endl;
                if(((Sum>>11) & 3) == 0)
                    Temp = k0;
                else if(((Sum>>11) & 3) == 1)
                    Temp = k1;
                else if (((Sum>>11) & 3) == 2)
                    Temp = k2;
                else Temp = k3;
                break;
            case ST_7:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_7" <<endl;
                V1 += ((((V0<<4) ^ (V0>>5)) + V0) ^ (Sum + Temp) );
                Counter.write(Counter.read() + 1);
                break;

            // decipher
            case ST_8:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_8" <<endl;
                Delta_mio = 0x9e3779b9;
                Sum = 0x13c6ef3720;
                break;
            case ST_9:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_9" <<endl;
                if(((Sum>>11) & 3) == 0)
                    Temp = k0;
                else if(((Sum>>11) & 3) == 1)
                    Temp = k1;
                else if (((Sum>>11) & 3) == 2)
                    Temp = k2;
                else Temp = k3;
                break;
            case ST_10:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_10" <<endl;
                V1 -= ((((V0<<4) ^ (V0>>5)) + V0) ^ (Sum + Temp) );
                break;
            case ST_11:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_11" <<endl;
                Sum -= Delta_mio;
                break;
            case ST_12:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_12" <<endl;
                if((Sum & 3) == 0)
                    Temp = k0;
                else if((Sum & 3) == 1)
                    Temp = k1;
                else if ((Sum & 3) == 2)
                    Temp = k2;
                else Temp = k3;
                break;
            case ST_13:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_13" <<endl;
                V0 -= ((((V1<<4) ^ (V1>>5)) + V1) ^ (Sum + Temp) );
                Counter.write(Counter.read() + 1);
                break;
            case ST_14:
            cout << "\t" << sc_time_stamp() << " - Elaborate: ST_14" <<endl;
                res0.write(V0);
                res1.write(V1);
                done.write(1);
                break;
            default:
                break;
        }
      }
}

/*
* Funzione per gestire la FSM
*/
void xtea_RTL::elaborate_XTEA_FSM(){
//    cout << "FSM: " << STATUS << " => ";
	NEXT_STATUS = STATUS;
    switch (STATUS) {
        case ST_0:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_0" <<endl;
            if (ready.read() == 1) {
                NEXT_STATUS = ST_1;
            } else {
                NEXT_STATUS = ST_0;
            }
            break;
        case ST_1:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_1" <<endl;
            if (mode.read() == 1) {
                NEXT_STATUS = ST_8;
            } else {
                NEXT_STATUS = ST_2;
            }
            break;
        case ST_2:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_2" <<endl;
            NEXT_STATUS = ST_3;
            break;
        case ST_3:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_3" <<endl;
            NEXT_STATUS = ST_4;
            break;
        case ST_4:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_4" <<endl;
            NEXT_STATUS = ST_5;
            break;
        case ST_5:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_5" <<endl;
            NEXT_STATUS = ST_6;
            break;
        case ST_6:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_6" <<endl;
            NEXT_STATUS = ST_7;
            break;
        case ST_7:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_7" <<endl;
            if (Counter.read() < 31) {
                NEXT_STATUS = ST_3;
            } else {
                NEXT_STATUS = ST_14;
            }
            break;
        case ST_8:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_8" <<endl;
            NEXT_STATUS = ST_9;
            break;
        case ST_9:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_9" <<endl;
            NEXT_STATUS = ST_10;
            break;
        case ST_10:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_10" <<endl;
            NEXT_STATUS = ST_11;
            break;
        case ST_11:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_11" <<endl;
            NEXT_STATUS = ST_12;
            break;
        case ST_12:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_12" <<endl;
        NEXT_STATUS = ST_13;
            break;
        case ST_13:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_13" <<endl;
            if (Counter.read() < 31) {
                NEXT_STATUS = ST_9;
            } else {
                NEXT_STATUS = ST_14;
            }
            break;

        case ST_14:
            cout << "\t" << sc_time_stamp() << " - FSM: ST_14" <<endl;
            NEXT_STATUS = ST_0;
            break;
    }
}
