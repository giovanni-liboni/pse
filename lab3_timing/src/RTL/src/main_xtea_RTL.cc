#include "xtea_RTL.hh"
#include "xtea_RTL_testbench.hh"

#define DEBUG 1
#define TRACE 1

int sc_main(int argc, char **argv)
{
    sc_clock                 clock;
    sc_signal < bool >		 reset;

    sc_signal<sc_uint<32> >  word0;
    sc_signal<sc_uint<32> >  word1;

    sc_signal<sc_uint<32> >  key0;
    sc_signal<sc_uint<32> >  key1;
    sc_signal<sc_uint<32> >  key2;
    sc_signal<sc_uint<32> >  key3;

    sc_signal<sc_uint<32> >  res0;
    sc_signal<sc_uint<32> >  res1;

  	sc_signal < bool >  	mode;
 	sc_signal < bool > 		ready;
 	sc_signal < bool >  	done;

 	xtea_RTL_testbench            testbench("src_RTL");      // testbench module
    xtea_RTL                      xtea("xtea_RTL");    // xtea RTL module

    testbench.clock(clock);
    testbench.reset(reset);
    testbench.mode(mode);
    testbench.ready(ready);
    testbench.done(done);

    testbench.word0(word0);
    testbench.word1(word1);

    testbench.key0(key0);
    testbench.key1(key1);
    testbench.key2(key2);
    testbench.key3(key3);

    testbench.res0(res0);
    testbench.res1(res1);

    // XTEA RTL module
    xtea.clk(clock);
    xtea.reset(reset);
    xtea.mode(mode);
    xtea.ready(ready);
    xtea.done(done);

    xtea.word0(word0);
    xtea.word1(word1);

    xtea.key0(key0);
    xtea.key1(key1);
    xtea.key2(key2);
    xtea.key3(key3);

    xtea.res0(res0);
    xtea.res1(res1);

#if TRACE
        sc_trace_file* Tf;
    	Tf = sc_create_vcd_trace_file("traces");
    	sc_trace(Tf, mode  , "mode" );
    	sc_trace(Tf, ready  , "ready" );
    	sc_trace(Tf, xtea.mode  , "xtea.mode" );
    	sc_trace(Tf, clock  , "clock" );
    	sc_trace(Tf, xtea.clk  , "xtea.clk" );
    	sc_trace(Tf, testbench.clock  , "testbench.clock" );
    	sc_trace(Tf, xtea.Delta_mio  , "xtea.Delta_mio" );
    	sc_trace(Tf, xtea.word0  , "xtea.word0" );
    	sc_trace(Tf, xtea.word1  , "xtea.word1" );
    	sc_trace(Tf, xtea.V0  , "xtea.V0" );
    	sc_trace(Tf, xtea.V1  , "xtea.V1" );
    	sc_trace(Tf, xtea.k0  , "xtea.k0" );
    	sc_trace(Tf, xtea.k1  , "xtea.k1" );
    	sc_trace(Tf, xtea.k2  , "xtea.k2" );
    	sc_trace(Tf, xtea.k3  , "xtea.k3" );
        sc_trace(Tf, xtea.done  , "xtea.done" );
#endif

	sc_start();  // run forever

#if TRACE
	sc_close_vcd_trace_file(Tf);
#endif
    return 0;
}
