#include "xtea_RTL_testbench.hh"

xtea_RTL_testbench::xtea_RTL_testbench(sc_module_name name_)
  : sc_module(name_)
{
  SC_THREAD(run);
	sensitive << clock.pos();
}

void xtea_RTL_testbench::xtea (uint32_t word0, uint32_t word1, uint32_t key0,
           uint32_t key1, uint32_t key2, uint32_t key3,
           bool mode,
           uint32_t * result0, uint32_t * result1) {

    uint64_t sum;
    uint32_t i, delta, v0, v1, temp;
    v0 = word0;
    v1 = word1;
    sum = 0;
    *result0 = 0;
    *result1 = 0;

    if(mode == 0) {
        // encipher
        delta=0x9e3779b9;
        for (i=0; i < 32; i++) {

            if((sum & 3) == 0)
                temp = key0;
            else if((sum & 3) == 1)
                temp = key1;
            else if ((sum & 3) == 2)
                temp = key2;
            else temp = key3;

            v0 += (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);

            sum += delta;

            if(((sum>>11) & 3) == 0)
                temp = key0;
            else if(((sum>>11) & 3) == 1)
                temp = key1;
            else if (((sum>>11) & 3) == 2)
                temp = key2;
            else temp = key3;

            v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);
        }
    }

    else if (mode == 1) {
        // decipher
        delta = 0x9e3779b9;
        sum = 0x13c6ef3720;
        for (i=0; i<32; i++) {

            if(((sum>>11) & 3) == 0)
                temp = key0;
            else if(((sum>>11) & 3) == 1)
                temp = key1;
            else if (((sum>>11) & 3) == 2)
                temp = key2;
            else temp = key3;

            v1 -= (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);

            sum -= delta;

            if((sum & 3) == 0)
                temp = key0;
            else if((sum & 3) == 1)
                temp = key1;
            else if ((sum & 3) == 2)
                temp = key2;
            else temp = key3;

            v0 -= (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);

        }
    }
    *result0 = v0;
    *result1 = v1;
}

void
xtea_RTL_testbench::run()
{
	sc_uint<32> r0, r1;

    uint32_t w0, w1, k0, k1, k2, k3;

#if DEBUG
    cout << "\t" << sc_time_stamp() << " - Inizio XTEA mode 1" <<endl;
#endif

    w0 = 0x12345678;
    w1 = 0x9abcdeff;

    k0 = 0x6a1d78c8;
    k1 = 0x8c86d67f;
    k2 = 0x2a65bfbe;
    k3 = 0xb4bd6e46;

    reset.write(1);

	mode.write(0);

    word0.write(w0);
    word1.write(w1);

    key0.write(k0);
    key1.write(k1);
    key2.write(k2);
    key3.write(k3);
    ready.write(1);

    wait();
#if DEBUG
    cout << "\t" << sc_time_stamp() << " - Word    : " << hex << word0.read() << word1.read() << endl;
    cout << "\t" << sc_time_stamp() << " - Key     : " << hex << key0.read() << key1.read() << key2.read() << key3.read() << endl;
#endif


	while(done.read() != 1){
		wait();
	}

	r0 = res0.read();
	r1 = res1.read();
#if DEBUG
    cout << "\t" << sc_time_stamp() << " - Fine XTEA mode 0" <<endl;
    cout << "\t" << sc_time_stamp() << " - Results : " << hex << res0.read() << res1.read() << endl;
#endif

    uint32_t result0, result1;

    xtea(w0, w1, k0, k1, k2, k3, false, &result0, &result1);


    if (r0 != result0 || r1 != result1) {
		cout << "Failed encryption" << endl;
	} else {
#if DEBUG
        cout << "\t" << sc_time_stamp() << " - All ok!" <<endl;
#endif
    }

    reset.write(0);
    wait();
#if DEBUG
    cout << "\t" << sc_time_stamp() << " - Start decryption mode 1" <<endl;
#endif

    reset.write(1);
    mode.write(1);

    // Se hanno passato il controllo precedente allora sono uguali a r0 e r1

    word0.write(r0);
    word1.write(r1);

    // Lascio la stessa chiave

    key0.write(k0);
    key1.write(k1);
    key2.write(k2);
    key3.write(k3);
    ready.write(1);

    wait();
#if DEBUG
    cout << "\t" << sc_time_stamp() << " - Word    : " << hex << word0.read() << word1.read() << endl;
    cout << "\t" << sc_time_stamp() << " - Key     : " << hex << key0.read() << key1.read() << key2.read() << key3.read() << endl;
#endif


    while(done.read() != 1){
        wait();
    }

    r0 = res0.read();
    r1 = res1.read();

#if DEBUG
    cout << "\t" << sc_time_stamp() << " - Fine XTEA mode 1" << endl;
    cout << "\t" << sc_time_stamp() << " - Results : " << hex << res0.read() << res1.read() << endl;
#endif
    xtea(result0, result1, k0, k1, k2, k3, true, &result0, &result1);

    if (r0 != result0 || r1 != result1) {
        cout << "Failed decryption" << endl;
    } else {
#if DEBUG
        cout << "\t" << sc_time_stamp() << " - Tutto ok!" << endl;
#endif
    }


    sc_stop();

}
