#include "xtea_AT4.hh"

xtea::xtea(sc_module_name name) : sc_module(name), socket("targetSocket"), pending_transaction(NULL){
    socket(*this);
    SC_THREAD(IOPROCESS);
}

void xtea::IOPROCESS()
{
    //Timing
    sc_time timing_annotation;

    while (true) {
        //Metto in pausa il processo, verrà riattivato durante una chiamata della nb_transport_fw
        wait(io_event);
        wait(50, SC_NS);
        cout << "\t" << sc_time_stamp() << " - Lancio IOPROCESS\n";

        //Controllo degli input in modo da stabilire la funzione da lanciare
        if (pending_transaction->is_write()){

            cout << "\t" << sc_time_stamp() << " - Ricevuta la richiesta per la b_transport write\n";

            if(xteaData.mode == 0){
                cout << "\t" << sc_time_stamp() << " - Chiamata la funzione di criptazione\n";
                encrypt();
            } else {
                cout << "\t" << sc_time_stamp() << " - Chiamata la funzione di decriptazione\n";
                decrypt();
            }

            //Rispondo la corretta transazione
            pending_transaction->set_response_status(tlm::TLM_OK_RESPONSE);
        }else {
            cout << "\t" << sc_time_stamp() << " - Il risultato della word0 è: "<<hex<<xteaData.result0<<endl;
            cout << "\t" << sc_time_stamp() << " - Il risultato della word1 è: "<<hex<<xteaData.result1<<endl;
        }

        //Rispondo la corretta transazione
        pending_transaction->set_response_status(tlm::TLM_OK_RESPONSE);

        *((structxtea*) pending_transaction->get_data_ptr()) = xteaData;
        tlm::tlm_phase phase = tlm::BEGIN_RESP;

        cout << "\t" << sc_time_stamp() << " - Chiamo la nb_transport_bw write\n";
        socket->nb_transport_bw(*pending_transaction, phase, timing_annotation);
        pending_transaction = NULL;
    }
}

tlm::tlm_sync_enum xtea::nb_transport_fw(tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& t){
    if (pending_transaction != NULL) {
        trans.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
        return tlm::TLM_COMPLETED;
    }

    if (phase != tlm::BEGIN_REQ) {
        trans.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
        return tlm::TLM_COMPLETED;
    }

    cout << "\t" << sc_time_stamp() << " - Ricevuta la richiesta per la nb_transport_fw\n";

    pending_transaction = &trans;
    xteaData = *((structxtea*) trans.get_data_ptr());

    phase = tlm::END_REQ;

    //Attivo lo IOPROCESS in modo che svolga l'algoritmo
    io_event.notify();

    //Ritorno il controllo al modulo chiamante
    cout << "\t" << sc_time_stamp() << " - La funzione nb_transport_fw è terminata\n";
    return tlm::TLM_UPDATED;
}

void xtea::encrypt() {
    uint64_t sum;
    uint32_t i, delta, v0, v1, temp, key0, key1, key2, key3;

    v0 = xteaData.word0;
    v1 = xteaData.word1;
    key0 = xteaData.key0;
    key1 = xteaData.key1;
    key2 = xteaData.key2;
    key3 = xteaData.key3;

    // encipher
    delta=0x9e3779b9;
    for (i=0; i < 32; i++) {
        sum += delta;

        if((sum & 3) == 0)
            temp = key0;
        else if((sum & 3) == 1)
            temp = key1;
        else if ((sum & 3) == 2)
            temp = key2;
        else temp = key3;

        v0 += (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);

        if(((sum>>11) & 3) == 0)
            temp = key0;
        else if(((sum>>11) & 3) == 1)
            temp = key1;
        else if (((sum>>11) & 3) == 2)
            temp = key2;
        else temp = key3;

        v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);
    }
    xteaData.result0 = v0;
    xteaData.result1 = v1;
}

void xtea::decrypt() {
    uint64_t sum;
    uint32_t i, delta, v0, v1, temp, key0, key1, key2, key3;

    v0 = xteaData.word0;
    v1 = xteaData.word1;
    key0 = xteaData.key0;
    key1 = xteaData.key1;
    key2 = xteaData.key2;
    key3 = xteaData.key3;

    // decipher
    delta = 0x9e3779b9;
    sum = 0x13c6ef3720;
    for (i=0; i<32; i++) {

        if(((sum>>11) & 3) == 0)
            temp = key0;
        else if(((sum>>11) & 3) == 1)
            temp = key1;
        else if (((sum>>11) & 3) == 2)
            temp = key2;
        else temp = key3;

        v1 -= (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);

        sum -= delta;

        if((sum & 3) == 0)
            temp = key0;
        else if((sum & 3) == 1)
            temp = key1;
        else if ((sum & 3) == 2)
            temp = key2;
        else temp = key3;

        v0 -= (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);

    }

    xteaData.result0 = v0;
    xteaData.result1 = v1;
}

unsigned int xtea::transport_dbg (tlm::tlm_generic_payload& trans)
{
    return 0;
}

bool xtea::get_direct_mem_ptr (tlm::tlm_generic_payload& trans, tlm::tlm_dmi& dmi_data)
{
    return false;
}

void xtea::b_transport (tlm::tlm_generic_payload& trans, sc_time& t)
{

}
