/* Libraries */
#include "xtea_AT4_testbench.hh"

//Costruttore
testBench :: testBench(sc_module_name name) : sc_module(name), response_pending(false) {

    //Inizializzazione del socket dell'initiator e creazione della tread per l'esecuzione della simulazione
    socket(*this);
    SC_THREAD(run);

}

//Esecuzione della simulazione
void testBench :: run() {

    cout<<sc_time_stamp()<<" - "<<name()<<" - run"<<endl;
    sc_time local_time;

    //Creo le strutture dati che mi serviranno per il dialogo con il tea.cpp
    structxtea xteaData;
    tlm :: tlm_generic_payload payload;

//###### --- PRIMA SIMULAZIONE --- ######
    cout << "------------------ INIZIO PRIMA SIMULAZIONE ------------------------ \n";
    //Implemento la struttura con i valori della prima simulazione
    xteaData.mode = 0;
    xteaData.word0 = 0x12345678;
    xteaData.word1 = 0x9abcdeff;
    xteaData.key0 = 0x6a1d78c8;
    xteaData.key1 = 0x8c86d67f;
    xteaData.key2 = 0x2a65bfbe;
    xteaData.key3 = 0xb4bd6e46;

    //Eseguo la prima simulazione
    //Attivo la fase per la richiesta
    tlm::tlm_phase phase = tlm::BEGIN_REQ;

    cout << "\t" << sc_time_stamp() << " - Calcolo della criptazione dei segnali\n";
    payload.set_data_ptr((unsigned char*) &xteaData);
    payload.set_address(0);

    //Richiedo al tea il calcolo dell'algoritmo
    payload.set_write();

    tlm::tlm_sync_enum result = socket->nb_transport_fw(payload, phase, local_time);

    //Controllo eventuali errori di transizione
    if (result == tlm::TLM_COMPLETED) {
        cout << "\t" << sc_time_stamp() << " - !!!!Si è verificato un errore, il sistema verrà terminato" << endl;
        sc_stop();
    }

    if (phase != tlm::END_REQ) {
        cout << "\t" << sc_time_stamp() << " - !!!!Si è verificato un errore, il sistema verrà terminato" << endl;
        sc_stop();
    }

    cout << "\t" << sc_time_stamp() << " - Attendo fino a che non viene invocata la nb_transport_bw\n";
    response_pending = true;
    wait(available_response);
    response_pending = false;


    phase = tlm::BEGIN_REQ;

    payload.set_address(0);
    payload.set_data_ptr((unsigned char*) &xteaData);
    payload.set_read();

    cout << "\t" << sc_time_stamp() << " - Richiedo il calcolo dell'algoritmo, modalità read\n";
    result = socket->nb_transport_fw(payload, phase, local_time);

    //Controllo eventuali errori di transizione
    if (result == tlm::TLM_COMPLETED) {
        cout << "\t" << sc_time_stamp() << " - !!!!Si è verificato un errore, il sistema verrà terminato" << endl;
        sc_stop();
    }

    if (phase != tlm::END_REQ) {
        cout << "\t" << sc_time_stamp() << " - !!!!Si è verificato un errore, il sistema verrà terminato" << endl;
        sc_stop();
    }

    cout << "\t" << sc_time_stamp() << " - Attendo fino a che non viene invocata la nb_transport_bw\n";
    response_pending = true;
    wait(available_response);
    response_pending = false;

    //Controllo che il tlm risponda correttamente alle richiesti (ovvero se dal tea è arrivato un TLM_OK_RESPONSE)
    if(payload.get_response_status() == tlm::TLM_OK_RESPONSE){
        //Stampo a video i risultati dell'algoritmo
        cout << "\t" << sc_time_stamp() << " - Risultati dell'algoritmo: \n";
        cout << "\t" << sc_time_stamp() << " - result0 = '" <<hex << xteaData.result0 << "' \n";
        cout << "\t" << sc_time_stamp() << " - result1 = '" <<hex << xteaData.result1 << "' \n";
    }
    cout << "------------------ FINE PRIMA SIMULAZIONE ------------------------ \n\n";

//###### --- SECONDA SIMULAZIONE --- ######
    cout << "------------------ INIZIO SECONDA SIMULAZIONE ------------------------ \n";

    //Implemento la struttura con i valori della seconda simulazione
    xteaData.mode = 1;
    xteaData.word0 = 0x12345678;
    xteaData.word1 = 0x9abcdeff;
    xteaData.key0 = 0x62ee209f;
    xteaData.key1 = 0x69b7afce;
    xteaData.key2 = 0x376a8936;
    xteaData.key3 = 0xcdc9e923;

    //Resetto la phase
    phase = tlm::BEGIN_REQ;

    //Eseguo la seconda simulazione
    cout << "\t" << sc_time_stamp() << " - Calcolo della decriptazione dei segnali\n";
    payload.set_data_ptr((unsigned char*) &xteaData);
    payload.set_address(0);

    //Richiedo al tea il calcolo dell'algoritmo
    payload.set_write();
    cout << "\t" << sc_time_stamp() << " - Richiedo il calcolo dell'algoritmo, modalità write\n";
    result = socket->nb_transport_fw(payload, phase, local_time);

    //Controllo eventuali errori di transizione
    if (result == tlm::TLM_COMPLETED) {
        cout << "\t" << sc_time_stamp() << " - !!!!Si è verificato un errore, il sistema verrà terminato" << endl;
        sc_stop();
    }

    if (phase != tlm::END_REQ) {
        cout << "\t" << sc_time_stamp() << " - !!!!Si è verificato un errore, il sistema verrà terminato" << endl;
        sc_stop();
    }

    cout << "\t" << sc_time_stamp() << " - Attendo fino a che non viene invocata la nb_transport_bw\n";
    response_pending = true;
    wait(available_response);
    response_pending = false;

    phase = tlm::BEGIN_REQ;
    payload.set_address(0);
    payload.set_data_ptr((unsigned char*) &xteaData);
    payload.set_read();

    cout << "\t" << sc_time_stamp() << " - Richiedo il calcolo dell'algoritmo, modalità read\n";
    result = socket->nb_transport_fw(payload, phase, local_time);

    //Controllo eventuali errori di transizione
    if (result == tlm::TLM_COMPLETED) {
        cout << "\t" << sc_time_stamp() << " - !!!!Si è verificato un errore, il sistema verrà terminato" << endl;
        sc_stop();
    }

    if (phase != tlm::END_REQ) {
        cout << "\t" << sc_time_stamp() << " - !!!!Si è verificato un errore, il sistema verrà terminato" << endl;
        sc_stop();
    }

    cout << "\t" << sc_time_stamp() << " - Attendo fino a che non viene invocata la nb_transport_bw\n";
    response_pending = true;
    wait(available_response);
    response_pending = false;

    //Controllo che il tlm risponda correttamente alle richiesti (ovvero se dal tea è arrivato un TLM_OK_RESPONSE)
    if(payload.get_response_status() == tlm::TLM_OK_RESPONSE){
        //Stampo a video i risultati dell'algoritmo
        cout << "\t" << sc_time_stamp() << " - Risultati dell'algoritmo: \n";
        cout << "\t" << sc_time_stamp() << " - result0 = '" <<hex << xteaData.result0 << "' \n";
        cout << "\t" << sc_time_stamp() << " - result1 = '" <<hex << xteaData.result1 << "' \n";
    }
    cout << "------------------ FINE SECONDA SIMULAZIONE ------------------------ \n\n";

    //Termino la thread di simulazione
    sc_stop();
}

//Non implementato ma richiesto per il protocollo TLM
void testBench :: invalidate_direct_mem_ptr(uint64 start_range, uint64 end_range){ }

//Conferma della chiusura della transizione
tlm::tlm_sync_enum testBench :: nb_transport_bw(tlm::tlm_generic_payload &  trans, tlm::tlm_phase &  phase, sc_time &  t) {
    //Gestisco eventuali errori di stato
    if (!response_pending) {
        cout << "\t" << sc_time_stamp() << " - !!! Si è verificato un errore riguardante nb_transport_be, la procedura sarà terminata" << endl;
        trans.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
        return tlm::TLM_COMPLETED;
    }
    if (phase != tlm::BEGIN_RESP) {
        cout << "\t" << sc_time_stamp() << " - !!! Si è verificato un errore riguardante nb_transport_be, la procedura sarà terminata" << endl;
        trans.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
        return tlm::TLM_COMPLETED;
    }

    //Sblocco l'iniziator
    available_response.notify();

    //Aggiorno il valore della phase in modo da completarela transazione
    phase = tlm::END_RESP;

    return tlm::TLM_COMPLETED;
}