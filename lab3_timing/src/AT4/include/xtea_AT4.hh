/* Define */
#pragma once

/* Libraries */
#include <systemc.h>
#include <tlm.h>
#include "define.h"
#include <stdint.h>

class xtea : public sc_module, public virtual tlm :: tlm_fw_transport_if<>{

public :

    //Dichiaro il socket
    tlm::tlm_target_socket<> socket;

    //Dichiarazione delle funzioni necessarie al protocollo TLM
    virtual void b_transport(tlm::tlm_generic_payload& trans, sc_time& t);
    virtual bool get_direct_mem_ptr(tlm::tlm_generic_payload& trans, tlm::tlm_dmi& dmi_data);
    virtual unsigned int transport_dbg(tlm::tlm_generic_payload& trans);

    virtual tlm::tlm_sync_enum nb_transport_fw(tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& t);

    //Dichiarazione dei componenti per le transazioni
    structxtea xteaData;
    tlm :: tlm_generic_payload* pending_transaction;
    sc_event io_event;

    //Attiva l'elaborazione del target quando il initiator lo risveglia
    void IOPROCESS();

    //Dichiarazione delle funzioni
    void encrypt();	//Esegue la criptazione del segnale
    void decrypt(); //Esegue la decriptazione del segnale

    SC_HAS_PROCESS(xtea);

    xtea(sc_module_name name);

};
