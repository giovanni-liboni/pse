#pragma once

#include <systemc.h>

// payload data struct declaration
struct structxtea {
    bool mode;
    sc_uint<32> word0;
    sc_uint<32> word1;
    sc_uint<32> key0;
    sc_uint<32> key1;
    sc_uint<32> key2;
    sc_uint<32> key3;
    sc_uint<32> result0;
    sc_uint<32> result1;
};
// constant declarations
#define ADDRESS_TYPE uint
#define DATA_TYPE structxtea
