/* Libraries */
#include "xtea_LT_testbench.hh"

//Costruttore
testBench :: testBench(sc_module_name name) : sc_module(name) {

    //Inizializzazione del socket dell'initiator e creazione della tread per l'esecuzione della simulazione
    socket(*this);
    SC_THREAD(run);

}

//Esecuzione della simulazione
void testBench :: run() {

    cout<<sc_time_stamp()<<" - "<<name()<<" - run"<<endl;

    sc_time local_time = m_qk.get_local_time();

    //Creo le strutture dati che mi serviranno per il dialogo con il tea.cpp
    structxtea xteaData;
    tlm :: tlm_generic_payload payload;

//###### --- PRIMA SIMULAZIONE --- ######
    cout << "------------------ INIZIO PRIMA SIMULAZIONE ------------------------ \n";
    //Implemento la struttura con i valori della prima simulazione
    xteaData.mode = 0;
    xteaData.word0 = 0x12345678;
    xteaData.word1 = 0x9abcdeff;
    xteaData.key0 = 0x6a1d78c8;
    xteaData.key1 = 0x8c86d67f;
    xteaData.key2 = 0x2a65bfbe;
    xteaData.key3 = 0xb4bd6e46;

    //Eseguo la prima simulazione
    cout << "\t" << sc_time_stamp() << " - Calcolo della criptazione dei segnali\n";
    payload.set_data_ptr((unsigned char*) &xteaData);
    payload.set_address(0);

    // Aggiorno il local time
    local_time = m_qk.get_local_time();

    //Richiedo al tea il calcolo dell'algoritmo
    payload.set_write();
    cout << "\t" << sc_time_stamp() << " - Richiedo il calcolo dell'algoritmo, modalità write\n";
    socket -> b_transport(payload, local_time);

    //Richiedo al tea il risultato dell'algoritmo (i result0 e result1)
    payload.set_read();

    cout << "\t" << sc_time_stamp() << " - Richiedo i risultati dell'algoritmo, modalità read\n";
    socket -> b_transport(payload, local_time);

    //Controllo che il tlm risponda correttamente alle richiesti (ovvero se dal tea è arrivato un TLM_OK_RESPONSE)
    if(payload.get_response_status() == tlm::TLM_OK_RESPONSE){
        //Stampo a video i risultati dell'algoritmo
        cout << "\t" << sc_time_stamp() << " - Risultati dell'algoritmo: \n";
        cout << "\t" << sc_time_stamp() << " - result0 = '" <<hex << xteaData.result0 << "' \n";
        cout << "\t" << sc_time_stamp() << " - result1 = '" <<hex << xteaData.result1 << "' \n";
    }
    //Calcolo il tempo di calcolo dell'algoritmo, e controllo la sincronizzazione tra testBench e tea
    cout << "\t" << sc_time_stamp() << " - Tempo totale: " << sc_time_stamp() << " + " << local_time << "\n";
    m_qk.set(local_time);
    if (m_qk.need_sync()) {
        cout << "\t" << sc_time_stamp() << " - Processi sincronizzati" << endl;
        m_qk.sync();
    }
    cout << "------------------ FINE PRIMA SIMULAZIONE ------------------------ \n\n";

//###### --- SECONDA SIMULAZIONE --- ######
    cout << "------------------ INIZIO SECONDA SIMULAZIONE ------------------------ \n";

    //Implemento la struttura con i valori della seconda simulazione
    xteaData.mode = 1;
    xteaData.word0 = 0x12345678;
    xteaData.word1 = 0x9abcdeff;
    xteaData.key0 = 0x62ee209f;
    xteaData.key1 = 0x69b7afce;
    xteaData.key2 = 0x376a8936;
    xteaData.key3 = 0xcdc9e923;

    //Eseguo la seconda simulazione
    cout << "\t" << sc_time_stamp() << " - Calcolo della decriptazione dei segnali\n";
    payload.set_data_ptr((unsigned char*) &xteaData);
    payload.set_address(0);

    //Aggiorno il local time
    local_time = m_qk.get_local_time();

    //Richiedo al tea il calcolo dell'algoritmo
    payload.set_write();
    cout << "\t" << sc_time_stamp() << " - Richiedo il calcolo dell'algoritmo, modalità write\n";
    socket -> b_transport(payload, local_time);

    //Richiedo al tea il risultato dell'algoritmo (i result0 e result1)
    payload.set_read();

    cout << "\t" << sc_time_stamp() << " - Richiedo i risultati dell'algoritmo, modalità read\n";
    socket -> b_transport(payload, local_time);

    //Controllo che il tlm risponda correttamente alle richiesti (ovvero se dal tea è arrivato un TLM_OK_RESPONSE)
    if(payload.get_response_status() == tlm::TLM_OK_RESPONSE){
        //Stampo a video i risultati dell'algoritmo
        cout << "\t" << sc_time_stamp() << " - Risultati dell'algoritmo: \n";
        cout << "\t" << sc_time_stamp() << " - result0 = '" <<hex << xteaData.result0 << "' \n";
        cout << "\t" << sc_time_stamp() << " - result1 = '" <<hex << xteaData.result1 << "' \n";
    }
    //Calcolo il tempo di calcolo dell'algoritmo, e controllo la sincronizzazione tra testBench e tea
    cout << "\t" << sc_time_stamp() << " - Tempo totale: " << sc_time_stamp() << " + " << local_time << "\n";
    m_qk.set(local_time);
    if (m_qk.need_sync()) {
        cout << "\t" << sc_time_stamp() << " - Processi sincronizzati" << endl;
        m_qk.sync();
    }

    cout << "------------------ FINE SECONDA SIMULAZIONE ------------------------ \n\n";

    //Termino la thread di simulazione
    sc_stop();
}

//Non implementato ma richiesto per il protocollo TLM
void testBench :: invalidate_direct_mem_ptr(uint64 start_range, uint64 end_range){ }

//Conferma della chiusura della transizione
tlm :: tlm_sync_enum testBench :: nb_transport_bw(tlm::tlm_generic_payload &  trans, tlm::tlm_phase &  phase, sc_time &  t) {
    return tlm::TLM_COMPLETED;
}