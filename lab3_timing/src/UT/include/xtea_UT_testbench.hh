#pragma once

/* Libraries */
#include <systemc.h>
#include <tlm.h>
#include <tlm_utils/tlm_quantumkeeper.h>
#include "define.h"


class testBench
        : public sc_module
                , public virtual tlm::tlm_bw_transport_if<>
{

private :

    SC_HAS_PROCESS(testBench);

    //Dichiarazione delle funzioni per il protocollo TLM
    virtual void invalidate_direct_mem_ptr(uint64 start_range, uint64 end_range);
    virtual tlm::tlm_sync_enum nb_transport_bw(tlm::tlm_generic_payload &  trans, tlm::tlm_phase &  phase, sc_time &  t);

    void run();

public:

    //Dichiarazione del soket dell'iniziator (chiamante)
    tlm::tlm_initiator_socket<> socket;

    //Dichiarazione del costruttore
    testBench(sc_module_name name);
};