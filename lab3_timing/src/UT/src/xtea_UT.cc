#include "xtea_UT.hh"

xtea::xtea(sc_module_name name) : sc_module(name), socket("targetSocket"), pending_transaction(NULL){
    socket(*this);
}
void xtea::b_transport( tlm :: tlm_generic_payload& trans, sc_time& t){

    xteaData = *((structxtea*) trans.get_data_ptr());

    //Controllo la transazione se in stato di write o read
    if (trans.is_write()) {
        cout << "\t" << sc_time_stamp() << " - Ricevuta la chiamata write della b_transport \n";
        //Se in write faccio il controllo sul tipo di mode che è arrivato, se 0 farò la crpitazione se 1 la decriptazione
        if(xteaData.mode == 0){
            cout << "\t" << sc_time_stamp() << " - Eseguo la criptazione del segnale \n";
            encrypt();
        }else{
            cout << "\t" << sc_time_stamp() << " - Eseguo la decriptazione del segnale \n";
            decrypt();
        }
        //Confermo l'avvenuta risposta del sistema alla transazione (ritorno TLM_OK_RESPONSE al modulo chiamante)
        trans.set_response_status( tlm :: TLM_OK_RESPONSE );
    }else if (trans.is_read()){
        //Ritorno i valori del risultato dell'algoritmo e li carico nel payload per reinviarli al modulo chiamante
        xteaData.result0 = result0;
        xteaData.result1 = result1;
        *((structxtea*) trans.get_data_ptr()) = xteaData;
        cout << "\t" << sc_time_stamp() << " -Results:\n";
        cout << "\t" << sc_time_stamp() << " - result0 : " <<hex << result0 << " \n";
        cout << "\t" << sc_time_stamp() << " - result1 : " <<hex << result1 << " \n";
    }
}

bool xtea::get_direct_mem_ptr(tlm::tlm_generic_payload& trans, tlm::tlm_dmi& dmi_data){
    return false;
}

tlm::tlm_sync_enum xtea::nb_transport_fw(tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& t){
    return tlm :: TLM_COMPLETED;
}

unsigned int xtea::transport_dbg(tlm::tlm_generic_payload& trans){
    return 0;
}

void xtea::encrypt() {
    uint64_t sum;
    uint32_t i, delta, v0, v1, temp, key0, key1, key2, key3;

    v0 = xteaData.word0;
    v1 = xteaData.word1;
    key0 = xteaData.key0;
    key1 = xteaData.key1;
    key2 = xteaData.key2;
    key3 = xteaData.key3;

    // encipher
    delta=0x9e3779b9;
    for (i=0; i < 32; i++) {
        sum += delta;

        if((sum & 3) == 0)
            temp = key0;
        else if((sum & 3) == 1)
            temp = key1;
        else if ((sum & 3) == 2)
            temp = key2;
        else temp = key3;

        v0 += (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);

        if(((sum>>11) & 3) == 0)
            temp = key0;
        else if(((sum>>11) & 3) == 1)
            temp = key1;
        else if (((sum>>11) & 3) == 2)
            temp = key2;
        else temp = key3;

        v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);
    }

    result0 = v0;
    result1 = v1;

}

void xtea::decrypt() {
    uint64_t sum;
    uint32_t i, delta, v0, v1, temp, key0, key1, key2, key3;

    v0 = xteaData.word0;
    v1 = xteaData.word1;
    key0 = xteaData.key0;
    key1 = xteaData.key1;
    key2 = xteaData.key2;
    key3 = xteaData.key3;

    // decipher
    delta = 0x9e3779b9;
    sum = 0x13c6ef3720;
    for (i=0; i<32; i++) {

        if(((sum>>11) & 3) == 0)
            temp = key0;
        else if(((sum>>11) & 3) == 1)
            temp = key1;
        else if (((sum>>11) & 3) == 2)
            temp = key2;
        else temp = key3;

        v1 -= (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);

        sum -= delta;

        if((sum & 3) == 0)
            temp = key0;
        else if((sum & 3) == 1)
            temp = key1;
        else if ((sum & 3) == 2)
            temp = key2;
        else temp = key3;

        v0 -= (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);

    }

    result0 = v0;
    result1 = v1;
}