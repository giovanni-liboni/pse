#pragma once

#include <systemc-ams.h>
#include <systemc.h>
#include "room.hh"
#include "RoomIFace.hh"
#include "heater.hh"
#include "HeaterIFace.hh"
#include "thermostat.hh"
#include "ThermostatIFace.hh"
#include "xtea_RTL.hh"
#include "xtea_RTL_transactor_ts.hh"
#include "xtea_RTL_transactor_plant.hh"
#include "xteaPlantIFace.hh"
#include "xteaTsIFace.hh"
#include "xtea_TLM_testbench_plant.hh"
#include "xtea_TLM_testbench_ts.hh"
#include "clk.hh"

class System : public sc_core::sc_module
{
public:
    System( sc_core::sc_module_name );
    ~System();
    sca_tdf::sca_signal< double > temp, voltage, current;
    sc_in_clk clock;

protected:
    // AMS
    room r;
    room_iface ri;
    heater h;
    heater_iface hi;

    thermostat t;
    thermostat_iface ti;

    // RTL
    xtea_RTL encipher, decipher;
    xteaPlantIFace _xteaPtIFace;
    xteaTsIFace _xteaTsIFace;

    // TLM
    xtea_RTL_transactor_ts encipherTransactor ;
    xtea_RTL_transactor_plant decipherTransactor;
    xtea_TLM_testbench_plant tb_pt;
    xtea_TLM_testbench_ts tb_ts;

    // Signals
    sca_tdf::sca_signal<double > sig_r_tdf_bw, sig_h_tdf_bw;
    sc_core::sc_signal<double > sig_tt_de_bw, sig_pt_de_bw;

    // XTEA signals Testbench side
    sc_core::sc_signal<sc_uint<32> > w0_ts, w1_ts, k0_ts, k1_ts, k2_ts, k3_ts, r0_ts, r1_ts;
    sc_core::sc_signal<bool > reset_ts, m_ts, r_ts, d_ts;

    // XTEA signals Heater/Room side
    sc_core::sc_signal<sc_uint<32> > w0_pt, w1_pt, k0_pt, k1_pt, k2_pt, k3_pt, r0_pt, r1_pt;
    sc_core::sc_signal<bool > reset_pt, m_pt, r_pt, d_pt;
};