#pragma once

#include <systemc-ams>

SCA_TDF_MODULE(instant_derivate_tmp)
{
    sca_tdf::sca_in<double> in;
    sca_tdf::sca_out<double> out;

    SCA_CTOR(instant_derivate_tmp);

    void set_attributes();
    void processing();
};
