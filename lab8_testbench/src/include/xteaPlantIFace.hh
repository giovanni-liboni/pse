#pragma once

#include "systemc.h"

SC_MODULE(xteaPlantIFace){

public:
    sc_in< sc_uint<32> > in0;
    sc_in< sc_uint<32> > in1;
    sc_in<bool > enable;
    unsigned int word[2];

    //output ports
    sc_out< double > out;

    void elaborate(void);

    SC_CTOR(xteaPlantIFace){
        SC_METHOD(elaborate);
        sensitive << enable;

        double zero = 0.0;
        word[0] = *(unsigned int*)(&zero);
        word[1] = *(((unsigned int*)(&zero)) + 1);

    };

};