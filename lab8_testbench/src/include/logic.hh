#pragma once

#include <systemc-ams>

#define DEFAULT 23

SCA_TDF_MODULE(logic)
{
    sca_tdf::sca_in<double> in_avg;
    sca_tdf::sca_in<double> in_id;
    sca_tdf::sca_out<double> out;

    SCA_CTOR(logic);

    void set_attributes();
    void processing();

};
