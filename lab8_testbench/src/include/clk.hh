#pragma once

#define PERIOD 1000
#include "systemc.h"


SC_MODULE(clk){
    sc_out< sc_dt::sc_logic > clock;

    void tick(void);

    SC_CTOR(clk){
        SC_THREAD(tick);
    };

};