#pragma once

#include <systemc.h>
#include <tlm.h>
#include <tlm_utils/tlm_quantumkeeper.h>
#include "define_LT.hh"

class xtea_TLM_testbench_plant: public sc_module, public virtual tlm::tlm_fw_transport_if<>
{
public:

    tlm::tlm_target_socket<> target_socket;

    iostruct  ioDataStruct;
    tlm::tlm_generic_payload* pending_transaction;
    sc_time timing_annotation;
    sc_time local_time;
    sc_event begin_write, end_write, begin_read, end_read;
    tlm_utils::tlm_quantumkeeper m_qk;

    //TLM interfaces
    virtual void b_transport(tlm::tlm_generic_payload& trans, sc_core::sc_time& t);
    virtual bool get_direct_mem_ptr(tlm::tlm_generic_payload& trans, tlm::tlm_dmi& dmi_data);
    virtual void invalidate_direct_mem_ptr(sc_dt::uint64 start_range, sc_dt::uint64 end_range);
    virtual tlm::tlm_sync_enum nb_transport_fw(tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& t);
    virtual unsigned int transport_dbg(tlm::tlm_generic_payload& trans);

    // constructor
    xtea_TLM_testbench_plant(sc_module_name name);

private:

    SC_HAS_PROCESS(xtea_TLM_testbench_plant);
};