#pragma once

/* Libraries */
#include <systemc.h>

/* Structure */
struct structxtea {
    //Definisco tutte le variabili che utilizzerò nel xtea
    bool mode;
    sc_uint<32> word0;
    sc_uint<32> word1;
    sc_uint<32> key0;
    sc_uint<32> key1;
    sc_uint<32> key2;
    sc_uint<32> key3;
    sc_uint<32> result0;
    sc_uint<32> result1;
};

#define ADDRESS_TYPE uint
#define DATA_TYPE tea_struct
