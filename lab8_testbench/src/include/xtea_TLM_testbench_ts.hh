#pragma once

#include <systemc.h>
#include <tlm.h>
#include <tlm_utils/tlm_quantumkeeper.h>
#include "define_LT.hh"

class xtea_TLM_testbench_ts : public sc_module, public virtual tlm::tlm_bw_transport_if<>
{
public:

    tlm::tlm_initiator_socket<> initiator_socket;
    tlm_utils::tlm_quantumkeeper m_qk;
    iostruct  ioDataStruct;

    xtea_TLM_testbench_ts(sc_module_name name);

    // Non implementate
    virtual void invalidate_direct_mem_ptr(uint64 start_range, uint64 end_range);
    virtual tlm::tlm_sync_enum nb_transport_bw(tlm::tlm_generic_payload &  trans, tlm::tlm_phase &  phase, sc_time &  t);

private:

    void run();

    SC_HAS_PROCESS(xtea_TLM_testbench_ts);
};