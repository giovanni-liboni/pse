#pragma once

#include "systemc.h"

SC_MODULE(xteaTsIFace){

public:
    sc_in< double > in;

    sc_out< sc_uint<32> > out0;
    sc_out< sc_uint<32> > out1;

    void elaborate(void);

    SC_CTOR(xteaTsIFace){

        SC_METHOD(elaborate);
        sensitive << in;
    };

};