#pragma once


/* Libraries */
#include <systemc.h>
#include <tlm.h>
#include "define.hh"
#include <tlm_utils/tlm_quantumkeeper.h>

class xtea_RTL_transactor_ts : public sc_module, public virtual tlm::tlm_fw_transport_if<>, public virtual tlm::tlm_bw_transport_if<>{

public:

    //Modulo TLM
    structxtea  xteaData;
    sc_time timing_annotation;
    sc_time local_time;
    sc_event begin_write, end_write, begin_read, end_read;
    tlm::tlm_target_socket<> target_socket;
    tlm::tlm_initiator_socket<> initiator_socket;
    tlm::tlm_generic_payload* pending_transaction, payload;

    tlm_utils::tlm_quantumkeeper m_qk;

    //Porte in ingresso dal RTL
    sc_in<sc_uint<32> > result0;
    sc_in<sc_uint<32> > result1;
    sc_in<bool>  done;

    //Porte in uscita verso il RTL
    sc_out<bool>  ready;
    sc_out<bool>  mode;
    sc_out<bool> s_reset;
    sc_out<sc_uint<32> > key0;
    sc_out<sc_uint<32> > key1;
    sc_out<sc_uint<32> > key2;
    sc_out<sc_uint<32> > key3;

    //Segnale di clock
    sc_in_clk clk;

    // Implementati
    virtual void b_transport(tlm::tlm_generic_payload& trans, sc_time& t);

    // Non implementati
    virtual unsigned int transport_dbg(tlm::tlm_generic_payload& trans);
    virtual bool get_direct_mem_ptr(tlm::tlm_generic_payload& trans, tlm::tlm_dmi& dmi_data);
    virtual tlm :: tlm_sync_enum nb_transport_fw(tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& t);

    virtual void invalidate_direct_mem_ptr(uint64 start_range, uint64 end_range);
    virtual tlm::tlm_sync_enum nb_transport_bw(tlm::tlm_generic_payload &  trans, tlm::tlm_phase &  phase, sc_time &  t);

    //Metodi di sincronizzazione con TLM
    void sync();
    void end_of_elaboration();
    void reset();

    //Processi del transactors per la comunicazione tra i moduli
    void WRITEPROCESS();
    void READPROCESS();

    //Processo transactors
    SC_HAS_PROCESS(xtea_RTL_transactor_ts);
    xtea_RTL_transactor_ts(sc_module_name name);
};
