#include "System.hh"


System::System( sc_core::sc_module_name ) :
        r("room"),
        ri("room_iface"),
        h("heater"),
        hi("heater_iface"),
        t("thermostat"),
        ti("thermostat_iface"),
        decipher("decipher"),
        encipher("encipher"),
        _xteaPtIFace("xteaPlantIFace"),
        _xteaTsIFace("xteaTsIFace"),
        decipherTransactor("decipherTransactor"),
        encipherTransactor("encipherTransactor"),
        tb_pt("xtea_TLM_testbench_plant"),
        tb_ts("xtea_TLM_testbench_ts")
{
    // Thermostat Interface Side
    t.out(current);
    ti.inc(current);

    // Thermostat Interface XTEA Side
    ti.outp(sig_tt_de_bw);
    _xteaTsIFace.in(sig_tt_de_bw);

    // XTEA Thermostat Side
    _xteaTsIFace.out0(w0_ts);
    _xteaTsIFace.out1(w1_ts);

    encipher.word0(w0_ts);
    encipher.word1(w1_ts);
    encipher.key0(k0_ts);
    encipher.key1(k1_ts);
    encipher.key2(k2_ts);
    encipher.key3(k3_ts);
    encipher.ready(r_ts);
    encipher.mode(m_ts);
    encipher.reset(reset_ts);
    encipher.res0(r0_ts);
    encipher.res1(r1_ts);
    encipher.done(d_ts);
    encipher.clk(clock);

    // Transactor Thermostat Side
    encipherTransactor.key0(k0_ts);
    encipherTransactor.key1(k1_ts);
    encipherTransactor.key2(k2_ts);
    encipherTransactor.key3(k3_ts);
    encipherTransactor.ready(r_ts);
    encipherTransactor.mode(m_ts);
    encipherTransactor.s_reset(reset_ts);
    encipherTransactor.result0(r0_ts);
    encipherTransactor.result1(r1_ts);
    encipherTransactor.done(d_ts);
    encipherTransactor.clk(clock);

    // Transactors Connection
    tb_ts.initiator_socket(encipherTransactor.target_socket);

    encipherTransactor.initiator_socket(decipherTransactor.target_socket);

    decipherTransactor.initiator_socket(tb_pt.target_socket);

    // Transactor Plant Side
    decipherTransactor.word0(w0_pt);
    decipherTransactor.word1(w1_pt);
    decipherTransactor.key0(k0_pt);
    decipherTransactor.key1(k1_pt);
    decipherTransactor.key2(k2_pt);
    decipherTransactor.key3(k3_pt);
    decipherTransactor.ready(r_pt);
    decipherTransactor.mode(m_pt);
    decipherTransactor.s_reset(reset_pt);
    decipherTransactor.done(d_pt);
    decipherTransactor.clk(clock);

    // XTEA Plant side
    decipher.word0(w0_pt);
    decipher.word1(w1_pt);
    decipher.key0(k0_pt);
    decipher.key1(k1_pt);
    decipher.key2(k2_pt);
    decipher.key3(k3_pt);
    decipher.ready(r_pt);
    decipher.mode(m_pt);
    decipher.reset(reset_pt);
    decipher.res0(r0_pt);
    decipher.res1(r1_pt);
    decipher.done(d_pt);
    decipher.clk(clock);

    // Xtea Plant IFace
    _xteaPtIFace.in0(r0_pt);
    _xteaPtIFace.in1(r1_pt);
    _xteaPtIFace.enable(d_pt);
    _xteaPtIFace.out(sig_pt_de_bw);

    // Plant Transactor Side
    hi.inp(sig_pt_de_bw);
    ri.inp_i(sig_pt_de_bw);

    // Heater Interface Side
    hi.outc(sig_h_tdf_bw);
    h.in(sig_h_tdf_bw);

    // Room Interface Side
    ri.outc_i(sig_r_tdf_bw);
    r.in_i(sig_r_tdf_bw);

    // Room -> Thermostat (Temperature)
    r.out(temp);
    t.in(temp);

    // Heater -> Room (Voltage)
    h.out(voltage);
    r.in_v(voltage);

    encipherTransactor.reset();
    decipherTransactor.reset();
}

System::~System()
{
}
