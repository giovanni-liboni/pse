#include "System.hh"

int main()
{
    sc_core::sc_set_time_resolution( 10, sc_core::SC_NS );
    sc_clock clock("Clock", 500, sc_core::SC_NS);

    System system( "system" );
    system.clock(clock);

    sca_util::sca_trace_file* atf = sca_util::sca_create_vcd_trace_file( "tr.vcd" );

    sca_util::sca_trace( atf, system.temp, "Temperature" );
    sca_util::sca_trace( atf, system.current, "Current" );
    sca_util::sca_trace( atf, system.voltage, "Voltage" );

    sc_start( 120, SC_SEC );
    sca_util::sca_close_vcd_trace_file( atf );
}