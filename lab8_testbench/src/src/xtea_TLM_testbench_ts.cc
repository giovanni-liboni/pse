#include "xtea_TLM_testbench_ts.hh"

xtea_TLM_testbench_ts::xtea_TLM_testbench_ts(sc_module_name name)
        : sc_module(name)
{
    initiator_socket(*this);
    SC_THREAD(run);

}

void xtea_TLM_testbench_ts::run()
{
    while(1)
    {
        sc_time local_time;
        tlm::tlm_generic_payload payload;

        // set data
        ioDataStruct.key0 = 0x6a1d78c8;
        ioDataStruct.key1 = 0x8c86d67f;
        ioDataStruct.key2 = 0x2a65bfbe;
        ioDataStruct.key3 = 0xb4bd6e46;

        payload.set_data_ptr((unsigned char*) &ioDataStruct);
        payload.set_address(0);
        payload.set_write();

        initiator_socket->b_transport(payload, local_time);
    }

}

void xtea_TLM_testbench_ts::invalidate_direct_mem_ptr(uint64 start_range, uint64 end_range)
{

}

tlm::tlm_sync_enum xtea_TLM_testbench_ts::nb_transport_bw(tlm::tlm_generic_payload &  trans, tlm::tlm_phase &  phase, sc_time &  t)
{
    return tlm::TLM_COMPLETED;
}