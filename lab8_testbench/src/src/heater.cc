#include "heater.hh"

heater::heater(sc_core::sc_module_name):
    in("in"),
    current_source("current_source"),
    out("out"),
    voltage_out_tdf("voltage_out_tdf"),
    r1("r1", 5.00),
    r2("r2", 5.00),
    gnd("gnd")
{
    current_source.inp(in);
    current_source.p(gnd);
    current_source.n(n1);

    r1.p(n1);
    r1.n(n2);

    r2.p(n2);
    r2.n(gnd);

    voltage_out_tdf.p(n1);
    voltage_out_tdf.n(gnd);
    voltage_out_tdf.outp(out);
}