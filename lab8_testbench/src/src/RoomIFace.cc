#include "RoomIFace.hh"

room_iface::room_iface(sc_core::sc_module_name) :
        outc_i( "outc_i" ),
        inp_i( "inp_i" )
{
}

room_iface::~room_iface()
{
}

void room_iface::processing()
{
    outc_i.write( inp_i.read() );
}