#include "xtea_RTL_transactor_plant.hh"

xtea_RTL_transactor_plant::xtea_RTL_transactor_plant(sc_module_name name) :
        sc_module(name),
        target_socket("target_socket"),
        initiator_socket("initiator_socket")

{
    target_socket(*this);
    initiator_socket(*this);
    //Creo la thread per il writeProcess sensibile al segnale di clock
    SC_THREAD(WRITEPROCESS);
    sensitive_pos << clk;
    //Creo la thread per il readProcess sensibile al segnale di clock
    SC_THREAD(READPROCESS);
    sensitive_pos << clk;
}

void xtea_RTL_transactor_plant::b_transport(tlm::tlm_generic_payload& trans, sc_time& t){
    wait(0, SC_NS);

    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> TLM_WRITE_COMMAND" << "\033[0m" << "\n";
    xteaData = *((structxtea*) trans.get_data_ptr());
    trans.set_response_status(tlm::TLM_OK_RESPONSE);
    begin_write.notify();
    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> Before WAIT(end_write)" << "\033[0m" << "\n";
    wait(end_write);
    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> After WAIT(end_write)" << "\033[0m" << "\n";

    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> TLM_READ_COMMAND" << "\033[0m" << "\n";
    xteaData = *((structxtea*) trans.get_data_ptr());
    trans.set_response_status(tlm::TLM_OK_RESPONSE);
    begin_read.notify();
    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> Before WAIT(end_read)" << "\033[0m" << "\n";
    wait(end_read);

    //Reset the keys
    xteaData.key0=0;
    xteaData.key1=0;
    xteaData.key2=0;
    xteaData.key3=0;

    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> After WAIT(end_read)" << "\033[0m" << "\n";
    *((structxtea*) trans.get_data_ptr()) = xteaData;

    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> End b_transport" << "\033[0m" << "\n";
}

//Terminated elaboration, reset signals
void xtea_RTL_transactor_plant::end_of_elaboration(){
    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> Start end_of_elaboration" << "\033[0m" << "\n";
    reset();
    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> End end_of_elaboration" << "\033[0m" << "\n";
}

//Reset signals
void xtea_RTL_transactor_plant::reset(){
    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> Start reset signal" << "\033[0m" << "\n";
    s_reset.write(0);
    ready.write(0);
    word0.write(0);
    word1.write(0);
    key0.write(0);
    key1.write(0);
    key2.write(0);
    key3.write(0);
    // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> End reset signal" << "\033[0m" << "\n";
}

//Write process implementation
void xtea_RTL_transactor_plant::WRITEPROCESS() {

    while (true) {
        wait(begin_write);
        // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> Start WRITEPROCESS" << "\033[0m" << "\n";
        s_reset.write(1);
        mode.write(1);
        // Decripto il risultato
        word0.write(xteaData.result0);
        word1.write(xteaData.result1);
        key0.write(xteaData.key0);
        key1.write(xteaData.key1);
        key2.write(xteaData.key2);
        key3.write(xteaData.key3);
        ready.write(1);
        end_write.notify();
        // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> End WRITEPROCESS" << "\033[0m" << "\n";
        wait();
    }
}

//Read process implementation
void xtea_RTL_transactor_plant::READPROCESS() {
    while (true) {
        wait(begin_read);
        // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> Start READPROCESS" << "\033[0m" << "\n";
        // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> Waiting done == 1" << "\033[0m" << "\n";
        while(done.read() != 1){
            wait();
        }
        // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> Now done = 1" << "\033[0m" << "\n";
        // cout << "\033[32m" << "   xtea_RTL_transactor_plant("<<sc_simulation_time()<<") -> End READPROCESS" << "\033[0m" << "\n";
        end_read.notify();
    }
}

//Other TLM functions
bool xtea_RTL_transactor_plant::get_direct_mem_ptr(tlm::tlm_generic_payload& trans, tlm::tlm_dmi& dmi_data) {
    return false;
}

tlm::tlm_sync_enum xtea_RTL_transactor_plant::nb_transport_fw(tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& t) {
    return tlm::TLM_UPDATED;
}

unsigned int xtea_RTL_transactor_plant::transport_dbg(tlm::tlm_generic_payload& trans) {
    return 0;
}

void xtea_RTL_transactor_plant::invalidate_direct_mem_ptr(uint64 start_range, uint64 end_range)
{
}

tlm::tlm_sync_enum xtea_RTL_transactor_plant::nb_transport_bw(tlm::tlm_generic_payload &  trans, tlm::tlm_phase &  phase, sc_time &  t)
{
    return tlm::TLM_UPDATED;
}