#include "clk.hh"

void clk :: tick(void){
    while(true)
    {
        clock.write( sc_dt::SC_LOGIC_1 );
        wait( PERIOD / 2, sc_core::SC_NS );
        clock.write( sc_dt::SC_LOGIC_0 );
        wait( PERIOD / 2, sc_core::SC_NS );
    }

}