#include "thermostat.hh"

thermostat::thermostat( sc_core::sc_module_name) :
    in("in"),
    out("out"),
    logicModule("logic"),
    avg_temp("avg_temp"),
    id_temp("id_temp")
{
    avg_temp.in(in);
    id_temp.in(in);
    avg_temp.out(avg_T);
    id_temp.out(dx_T);
    logicModule.in_avg(avg_T);
    logicModule.in_id(dx_T);
    logicModule.out(out);
}

