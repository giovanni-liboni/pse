#include "xtea_TLM_testbench_plant.hh"

xtea_TLM_testbench_plant::xtea_TLM_testbench_plant(sc_module_name name)
        : sc_module(name)
        , target_socket("target_socket")
{
    target_socket(*this);
}



void xtea_TLM_testbench_plant::b_transport(tlm::tlm_generic_payload& trans, sc_time& t)
{
    wait(0, SC_NS);
    ioDataStruct = *((iostruct*) trans.get_data_ptr());
    trans.set_response_status(tlm::TLM_OK_RESPONSE);

    // set data
    ioDataStruct.key0 = 0x6a1d78c8;
    ioDataStruct.key1 = 0x8c86d67f;
    ioDataStruct.key2 = 0x2a65bfbe;
    ioDataStruct.key3 = 0xb4bd6e46;

    *((iostruct*) trans.get_data_ptr()) = ioDataStruct;
}

bool xtea_TLM_testbench_plant::get_direct_mem_ptr(tlm::tlm_generic_payload& trans, tlm::tlm_dmi& dmi_data)
{
    return false;
}

unsigned int xtea_TLM_testbench_plant::transport_dbg(tlm::tlm_generic_payload& trans)
{
    return 0;
}
void xtea_TLM_testbench_plant::invalidate_direct_mem_ptr(sc_dt::uint64 start_range, sc_dt::uint64 end_range)
{

}

tlm::tlm_sync_enum xtea_TLM_testbench_plant::nb_transport_fw(tlm::tlm_generic_payload &  trans, tlm::tlm_phase &  phase, sc_time &  t)
{
    return tlm::TLM_COMPLETED;
}