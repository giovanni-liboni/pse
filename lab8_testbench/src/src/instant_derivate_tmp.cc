#include "instant_derivate_tmp.hh"

instant_derivate_tmp::instant_derivate_tmp(sc_core::sc_module_name) :
    in("in"),
    out("out")
{

}

void instant_derivate_tmp::set_attributes()
{
    in.set_rate(2);
    out.set_rate(1);
}

void instant_derivate_tmp::processing()
{
	out.write(in.read(1) - in.read(0));
}
