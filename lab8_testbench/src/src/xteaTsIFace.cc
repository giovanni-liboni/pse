#include "xteaTsIFace.hh"

void xteaTsIFace::elaborate(void)
{
    double val = in.read();
    unsigned int w0 = *(unsigned int*)(&val);
    unsigned int w1 = *(((unsigned int*)(&val)) + 1);
    // double-to-int
    out0.write(w0);
    out1.write(w1);
}