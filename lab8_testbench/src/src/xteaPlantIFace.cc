#include "xteaPlantIFace.hh"

void xteaPlantIFace::elaborate(void)
{
    // some magic here
    // remember: bits are bits!
    if(enable.read() == 1)
    {
        word[0] = in0.read();
        word[1] = in1.read();
    }
    // int-to-double
    out.write(*(double*) word);
}