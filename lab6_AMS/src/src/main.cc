#include <systemc.h>

#include "thermostat.hh"
#include "heater.hh"
#include "room.hh"


int sc_main( int argc, char * argv[] )
{
    sc_core::sc_set_time_resolution( 1, sc_core::SC_FS );

    sca_tdf::sca_signal <double> current;
    sca_tdf::sca_signal <double> voltage;
    sca_tdf::sca_signal <double> temperature;

    thermostat t("ts");
    t.in(temperature);
    t.out(current);

    heater h("hea");
    h.inp(current);
    h.outp(voltage);

    room r("r");
    r.in_i(current);
    r.in_v(voltage);
    r.out(temperature);

    sca_util::sca_trace_file* stf = sca_util::sca_create_vcd_trace_file( "tr.vcd" );

    sca_util::sca_trace( stf, temperature, "temperature" );
    sca_util::sca_trace( stf, current, "current" );
    sca_util::sca_trace( stf, voltage, "voltage" );

    sca_util::sca_trace( stf, t.avg_temp.out, "avg" );
    sca_util::sca_trace( stf, t.id_temp.out, "id" );


    sc_core::sc_start( 360, sc_core::SC_SEC );
    sca_util::sca_close_vcd_trace_file( stf );

    return 0;
}
